package com.hermansendev.a01_tottenromatoes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.models.PictureAssets;

import java.util.ArrayList;

public class MoviesAdapter extends BaseAdapter {

    Context context;
    ArrayList<Movie> movieList;
    Movie movie;

    public MoviesAdapter(Context context, ArrayList<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < movieList.size()){
            return movieList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // If view is null, inflate a new one
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_list_item, null);
        }

        // Extract movie from position
        movie = movieList.get(position);

        if (movie != null){
            TextView txtTitle = (TextView) convertView.findViewById(R.id.list_item_movies_title);
            txtTitle.setText(movie.getTitle());

            ImageView picture = (ImageView) convertView.findViewById(R.id.list_item_movies_image);
            // Get image based of primary genre (first genre) from movie
            Drawable image = context.getResources().getDrawable(
                    PictureAssets.getPicture(
                            movie.getGenres().get(0)
                    ));
            picture.setImageDrawable(image);

            TextView txtWatched = (TextView) convertView.findViewById(R.id.list_item_movies_watched);
            txtWatched.setText(movie.isWatched() ? context.getString(R.string.watched) : "");

            TextView txtRating = (TextView) convertView.findViewById(R.id.list_item_movies_rating);
            txtRating.setText(Float.toString(movie.getRating()));

            TextView txtUserRating = (TextView) convertView.findViewById(R.id.list_item_movies_userrating);
            txtUserRating.setText(String.format("%.1f", movie.getUserRating()));
        }

        return convertView;
    }
}

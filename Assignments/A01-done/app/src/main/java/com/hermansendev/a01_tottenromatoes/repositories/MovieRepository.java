package com.hermansendev.a01_tottenromatoes.repositories;

import com.hermansendev.a01_tottenromatoes.R;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.services.MoviesLoader;

import java.io.InputStream;
import java.util.ArrayList;

public class MovieRepository implements IMovieRepository {

    // Collection of movies, should be replaced with dbContext
    private ArrayList<Movie> movies;

    public MovieRepository(InputStream inputStream) {
        // Load db from raw csv
        ArrayList rawArray = MoviesLoader.LoadMovies(inputStream);
        // Parse csv format to object
        movies = MoviesLoader.parseCSVToMovies(rawArray);
    }

    /**
     * Add a movie to the db
     * @param movie
     */
    @Override
    public void AddMovie(Movie movie){
        movies.add(movie);
    }

    /**
     * Returns a clone of the object, this is to emulate an enumerable.
     * @return
     */
    @Override
    public ArrayList<Movie> GetAll(){
        return (ArrayList<Movie>)movies.clone();
    }

    /**
     * Get movie from index
     * @param position
     * @return
     */
    @Override
    public Movie GetMovie(int position) {
        if (position < movies.size()){
            return movies.get(position);
        } else {
            return null;
        }
    }


    /**
     * Replace a movie at index
     * @param position
     * @param movie
     */
    @Override
    public void EditMovie(int position, Movie movie){
        if (position < movies.size() && movie != null){
            movies.set(position, movie);
        }
    }

    /**
     * Delete a movie at index
     * @param position
     */
    @Override
    public void deleteMovie(int position){
        if (position < movies.size()){
            movies.remove(position);
        }
    }
}

package com.hermansendev.a01_tottenromatoes.models;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import com.hermansendev.a01_tottenromatoes.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class Movie implements IMovie {
    // Official
    private String Title = "";
    private String Plot = "";
    private ArrayList<String> Genres = new ArrayList<String>();
    private float Rating = 5;
    private String Picture = "";

    // User
    private String Comment = "";
    private float UserRating = 5;
    private boolean Watched = false;

    public boolean isWatched() {
        return Watched;
    }

    public void setWatched(boolean watched) {
        Watched = watched;
    }

    public float getUserRating() {
        return UserRating;
    }

    public void setUserRating(float userRating) {
        UserRating = userRating;
    }

    @Override
    public String getComment() {
        return Comment;
    }

    @Override
    public void setComment(String comment) {
        Comment = comment;
    }

    @Override
    public float getRating() {
        return Rating;
    }

    @Override
    public String getPicture() {
        return Picture;
    }

    @Override
    public void setPicture(String picture) {
        Picture = picture;
    }

    @Override
    public void setRating(float rating) {
        Rating = rating;
    }

    @Override
    public ArrayList<String> getGenres() {
        return Genres;
    }

    @Override
    public void setGenres(ArrayList<String> genres) {
        Genres = genres;
    }

    @Override
    public String getPlot() {
        return Plot;
    }

    @Override
    public void setPlot(String plot) {
        Plot = plot;
    }

    @Override
    public String getTitle() {
        return Title;
    }

    @Override
    public void setTitle(String title) {
        Title = title;
    }

    // To fix problems with parsing Class to persistence
    public void AddToIntent(Intent intent, Context context){

        intent.putExtra(context.getString(R.string.movie_extra_title), this.getTitle());
        intent.putExtra(context.getString(R.string.movie_extra_plot), this.getPlot());
        intent.putStringArrayListExtra(context.getString(R.string.movie_extra_genres), this.getGenres());
        intent.putExtra(context.getString(R.string.movie_extra_rating), this.getRating());
        intent.putExtra(context.getString(R.string.movie_extra_picture), this.getPicture());
        intent.putExtra(context.getString(R.string.movie_extra_comment), this.getComment());
        intent.putExtra(context.getString(R.string.movie_extra_userRating), this.getUserRating());
        intent.putExtra(context.getString(R.string.movie_extra_watched), this.isWatched());
    }

    // To fix problems with parsing Class to persistence
    public void GetFromIntent(Intent intent, Context context){
        if (intent.hasExtra(context.getString(R.string.movie_extra_title))) {
            setTitle(intent.getExtras().getString(context.getString(R.string.movie_extra_title)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_plot))) {
            setPlot(intent.getExtras().getString(context.getString(R.string.movie_extra_plot)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_genres))) {
            setGenres(intent.getExtras().getStringArrayList(context.getString(R.string.movie_extra_genres)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_rating))) {
            setRating(intent.getExtras().getFloat(context.getString(R.string.movie_extra_rating)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_picture))) {
            setPicture(intent.getExtras().getString(context.getString(R.string.movie_extra_picture)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_comment))) {
            setComment(intent.getExtras().getString(context.getString(R.string.movie_extra_comment)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_userRating))) {
            setUserRating(intent.getExtras().getFloat(context.getString(R.string.movie_extra_userRating)));
        }
        if (intent.hasExtra(context.getString(R.string.movie_extra_watched))) {
            setWatched(intent.getExtras().getBoolean(context.getString(R.string.movie_extra_watched)));
        }
    }
}
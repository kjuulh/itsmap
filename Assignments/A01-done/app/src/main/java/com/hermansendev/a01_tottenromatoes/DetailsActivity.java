package com.hermansendev.a01_tottenromatoes;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.models.PictureAssets;
import com.hermansendev.a01_tottenromatoes.repositories.MovieRepository;
import com.hermansendev.a01_tottenromatoes.services.MoviesJSON;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DetailsActivity extends AppCompatActivity {

    // Primary movie, that the view is based of
    Movie movie;

    ImageView image;
    TextView title;
    TextView rating;
    TextView userRating;
    TextView plot;
    TextView comment;
    TextView genres;
    CheckBox watched;
    Button okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        initializeView();
        // Check if it's a new activity, or if it is returning from a rotation or pause/stop
        if (savedInstanceState == null){
            // Handle startup from another activity
            handleIntent();
        } else {
            // Handle rotation and or pause.
            handleShortTermMemory(savedInstanceState);
        }
        setDetailsInfo();
        setOnClickListeners();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Put movie in json string in savedstate
        MoviesJSON json = new MoviesJSON();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        String jsonStr = "";
        try {
            json.WriteMovieJsonStream(os, this.movie);
            // Stream to local string
            jsonStr = os.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save in persistence on a resource specifier
        outState.putString(getString(R.string.details_key), jsonStr);
    }

    /**
     * Handle rotations, pause.
     * @param savedInstanceState
     */
    private void handleShortTermMemory(Bundle savedInstanceState) {
        // Get movie in json from savedstate
        MoviesJSON json = new MoviesJSON();
        String jsonStr = savedInstanceState.getString(getString(R.string.details_key));
        InputStream is = new ByteArrayInputStream(jsonStr.getBytes());
        try {
            this.movie = json.readMovieJsonStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handle intent sent from another activity, and extracts movie object
     */
    private void handleIntent() {
        // Get intent from other activity
        Intent i = getIntent();
        movie = new Movie();
        // Get class from intent
        movie.GetFromIntent(i, this);
    }

    /**
     * Fill view with information
     */
    private void setDetailsInfo() {

        // Get asset based on primary genre
        Drawable imageDrawable = this.getResources().getDrawable(
                PictureAssets.getPicture(
                        movie.getGenres().get(0)
                ));
        image.setImageDrawable(imageDrawable);

        title.setText(movie.getTitle());
        rating.setText(Float.toString(movie.getRating()));
        // set precision on user specified ratings, this should mitigate float inconsistencies
        userRating.setText(String.format("%.1f", movie.getUserRating()));
        plot.setText(movie.getPlot());
        comment.setText(movie.getComment());

        // Set genres from array
        StringBuilder tempGenre = new StringBuilder();
        for (int j = 0; j < movie.getGenres().size() - 1; j++){
            tempGenre.append(movie.getGenres().get(j)).append(", ");
        }
        tempGenre.append(movie.getGenres().get(movie.getGenres().size() - 1));
        genres.setText(tempGenre.toString());
        watched.setChecked(movie.isWatched());
    }

    /**
     * Initialize button(s)
     */
    private void setOnClickListeners() {
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOk();
            }
        });
    }

    /**
     * Return to the previous activity with rc OK
     */
    private void handleOk() {
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Get objects in view
     */
    private void initializeView() {
        image = findViewById(R.id.img_details_image);
        title = findViewById(R.id.txt_details_title);
        rating = findViewById(R.id.txt_details_rating);
        userRating = findViewById(R.id.txt_details_user_rating);
        plot = findViewById(R.id.txt_details_plot);
        comment = findViewById(R.id.txt_details_user_comment);
        genres = findViewById(R.id.txt_details_genres);
        watched = findViewById(R.id.check_details_watched);
        okButton = findViewById(R.id.btn_details_ok);
    }
}

package com.hermansendev.a01_tottenromatoes;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.repositories.MovieRepository;
import com.hermansendev.a01_tottenromatoes.services.MoviesJSON;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class OverviewActivity extends AppCompatActivity {

    private ListView moviesListView;
    private MoviesAdapter moviesAdapter;

    private MovieRepository movieRepository;
    private ArrayList<Movie> movies = new ArrayList<Movie>();

    Button exitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        // Handles rotation and pause
        handleShortTermMemory(savedInstanceState);

        // Get movies
        InitializeMovies();

        // Get widgets from view
        initializeWidgets();

        // sets listeners
        setOnClickListeners();
    }

    /**
     * Gets widgets to handle in code
     */
    private void initializeWidgets() {
        exitBtn = findViewById(R.id.btn_overview_exit);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save movieslist as json in state
        MoviesJSON json = new MoviesJSON();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        String jsonStr = "";
        try {
            json.WriteMoviesJsonStream(os, this.movies);
             jsonStr = os.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outState.putString(getString(R.string.overview_key), jsonStr);
    }

    /**
     * Get movies list from json in state
     * @param savedInstanceState
     */
    private void handleShortTermMemory(Bundle savedInstanceState) {
        if (savedInstanceState == null)  {
            // if null, then it's because it's a new class, then load from repository
            movieRepository = new MovieRepository(getResources().openRawResource(R.raw.movielist));
            if (movies.size() == 0){
                movies = movieRepository.GetAll();
            }
        } else {
            // else load from state
            MoviesJSON json = new MoviesJSON();
            String jsonStr = savedInstanceState.getString(getString(R.string.overview_key));
            InputStream is = new ByteArrayInputStream(jsonStr.getBytes());
            try {
                this.movies = json.readMoviesJsonStream(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void InitializeMovies() {
        // get adapter and set movies
        moviesAdapter = new MoviesAdapter(this, movies);
        moviesListView = (ListView)findViewById(R.id.list_overview_movies);
        moviesListView.setAdapter(moviesAdapter);
    }

    private void setOnClickListeners() {
        moviesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleNormalClick(parent, view, position, id);
            }
        });
        moviesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return handleLongClick(parent, view, position, id);
            }
        });
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleExit();
            }
        });
    }

    /**
     * Exits the process (there might be a more graceful method)
     */
    private void handleExit() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    /**
     * Handle a normal click on a movie (detail view)
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    private void handleNormalClick(AdapterView<?> parent, View view, int position, long id) {
        // get intent for detailview
        Intent startDetailIntent = new Intent();
        // get position
        startDetailIntent.putExtra(getString(R.string.resource_position), position);

        // add position and add to intent
        movies.get(position).AddToIntent(startDetailIntent, this);
        String action = getString(R.string.action_details);
        int resultCode = getResources().getInteger(R.integer.request_details);

        // set action and start activity
        if (action != null && !action.equals("")){
            startDetailIntent.setAction(action);
            startActivityForResult(startDetailIntent, resultCode);
        }
    }

    /**
     * handle a looong click on a movie (edit view)
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    private boolean handleLongClick(AdapterView<?> parent, View view, int position, long id) {
        // get intent for editview
        Intent startEditIntent = new Intent();
        // get position
        startEditIntent.putExtra(getString(R.string.resource_position), position);

        // Put movie position
        movies.get(position).AddToIntent(startEditIntent, this);
        String action = getString(R.string.action_edit);
        int resultCode = getResources().getInteger(R.integer.request_edit);

        // Start activity
        if (action != null && !action.equals("")){
            startEditIntent.setAction(action);
            startActivityForResult(startEditIntent, resultCode);
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        // From where did we return
        if (requestCode == getInteger(R.integer.request_details)){ // handle origin detailview
            switch (resultCode){
                case RESULT_OK: // if okay
                    handleDetailsReturnSuccess(data);
                    break;
                case RESULT_CANCELED:
                    handleDetailsReturnCancelled(data);
            }
        } else if(requestCode == getInteger(R.integer.request_edit)) { // handle origin editview
            switch (resultCode){
                case RESULT_OK: // if okay
                    handleEditReturnSuccess(data);
                    break;
                case RESULT_CANCELED:
                    handleEditReturnCancelled(data);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleDetailsReturnSuccess(Intent data) {
        //Toast.makeText(this, "details success", Toast.LENGTH_SHORT).show();
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleDetailsReturnCancelled(Intent data) {
        //Toast.makeText(this, "details cancelled", Toast.LENGTH_SHORT).show();
    }

    /**
     * gets movie from edit activity and updates view
     * @param data
     */
    private void handleEditReturnSuccess(Intent data) {
        //Toast.makeText(this, "edit success", Toast.LENGTH_SHORT).show();
        Movie movie = new Movie();
        movie.GetFromIntent(data, this);
        movies.set(data.getExtras().getInt(getString(R.string.position_var)), movie);
        InitializeMovies();
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleEditReturnCancelled(Intent data) {
        //Toast.makeText(this, "edit cancelled", Toast.LENGTH_SHORT).show();
    }

    // returns integer based of resources
    private int getInteger(int resId) {
        return getResources().getInteger(resId);
    }
}

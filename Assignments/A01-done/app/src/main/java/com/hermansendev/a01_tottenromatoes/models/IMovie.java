package com.hermansendev.a01_tottenromatoes.models;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

public interface IMovie {
    String getComment();

    void setComment(String comment);

    float getRating();

    String getPicture();

    void setPicture(String picture);

    void setRating(float rating);

    ArrayList<String> getGenres();

    void setGenres(ArrayList<String> genres);

    String getPlot();

    void setPlot(String plot);

    String getTitle();

    void setTitle(String title);

    void AddToIntent(Intent intent, Context context);

    void GetFromIntent(Intent intent, Context context);
}

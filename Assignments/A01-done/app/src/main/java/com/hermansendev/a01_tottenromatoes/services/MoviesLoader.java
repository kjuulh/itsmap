package com.hermansendev.a01_tottenromatoes.services;

import com.hermansendev.a01_tottenromatoes.models.IMovie;
import com.hermansendev.a01_tottenromatoes.models.Movie;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MoviesLoader {

    /**
     * Loading a movies from json. Splits at ';'
     *
     * @param inputStream
     * @return
     */
    public static ArrayList LoadMovies(InputStream inputStream) {

        ArrayList resultList = new ArrayList();
        // Buffered reader based on stream
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {

            String csvLine;
            // Get each row of the csv file
            while ((csvLine = reader.readLine()) != null) {
                // Split each row in string format, to seperate strings
                resultList.add(csvLine.split(";"));
            }
        } catch (IOException e) {
            throw new RuntimeException("Error in reading CSV file: " + e);
        } finally {
            try {
                // Cleanup stream
                inputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("Error in closing input stream: " + e);
            }
        }
        return resultList;
    }

    /**
     * Parse string array to movies objects
     *
     * @param raw
     * @return
     */
    public static ArrayList<IMovie> parseCSVToMovies(ArrayList<String[]> raw) {

        // remove the header
        raw.remove(0);
        ArrayList<IMovie> movies = new ArrayList<IMovie>();

        // Each row
        for (String[] row :
                raw) {
            IMovie movie = new Movie();

            movie.setTitle(row[0]);
            movie.setPlot(row[1]);
            // get genres as array
            movie.setGenres(new ArrayList<String>(Arrays.asList(row[2].split(", "))));
            // parse to float
            movie.setRating(Float.valueOf(row[3]));

            movies.add(movie);
        }

        return movies;
    }
}

package com.hermansendev.a01_tottenromatoes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.services.MoviesJSON;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class EditActivity extends AppCompatActivity {

    // Primary movie that the view interacts with
    Movie movie;

    TextView title;
    TextView userRating;
    SeekBar userRatingSlider;
    CheckBox watched;
    EditText comment;
    Button ok;
    Button cancel;

    // Initialize position to negative, this is a non-natural index
    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        // get objects from view
        initializeView();

        // handle origin of where to extract information
        if (savedInstanceState == null) {
            // handle information from another activity
            handleIntent();
        } else {
            // handle savedstate from rotation or pause
            handleShortTermMemory(savedInstanceState);
        }
        // set information
        setEditInfo();
        // set listeners
        setOnClickListeners();
    }

    /**
     * Set view information, based of movie
     */
    private void setEditInfo() {
        title.setText(movie.getTitle());
        // set userRating based of float precision 0.1
        userRating.setText(String.format("%.1f", movie.getUserRating()));
        userRatingSlider.setProgress((int) movie.getUserRating() * 10);
        watched.setChecked(movie.isWatched());
        comment.setText(movie.getComment());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save movie as json string in state
        MoviesJSON json = new MoviesJSON();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        String jsonStr = "";
        try {
            json.WriteMovieJsonStream(os, this.movie);
            jsonStr = os.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        outState.putString(getString(R.string.edit_key), jsonStr);
    }

    /**
     * Load movie from state
     *
     * @param savedInstanceState
     */
    private void handleShortTermMemory(Bundle savedInstanceState) {
        MoviesJSON json = new MoviesJSON();
        String jsonStr = savedInstanceState.getString(getString(R.string.edit_key));
        InputStream is = new ByteArrayInputStream(jsonStr.getBytes());
        try {
            this.movie = json.readMovieJsonStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handle intent from another class
     */
    private void handleIntent() {
        Intent i = getIntent();
        movie = new Movie();
        movie.GetFromIntent(i, this);
        position = i.getExtras().getInt(getString(R.string.position_var));
    }

    /**
     * Set all listeners
     */
    private void setOnClickListeners() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOkClick();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancelClick();
            }
        });
        userRatingSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                handleProgress(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        watched.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                handleCheckWatched(isChecked);
            }
        });
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleEditText(s.toString());
            }
        });
    }

    /**
     * Set user comment in movie
     *
     * @param text
     */
    private void handleEditText(String text) {
        movie.setComment(text);
    }

    /**
     * set watched status in movie
     *
     * @param isChecked
     */
    private void handleCheckWatched(boolean isChecked) {
        movie.setWatched(isChecked);
    }

    /**
     * Convert progress into userrating with precision 0.1
     *
     * @param progress
     */
    private void handleProgress(int progress) {
        Float value = progress * 0.1f;
        movie.setUserRating(value);
        userRating.setText(String.format("%.1f", value));
    }

    /**
     * Returns a cancelled result to previous activity
     */
    private void handleCancelClick() {
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * Persists movie object and position, then returns OK result
     */
    private void handleOkClick() {
        Intent i = new Intent();
        movie.AddToIntent(i, this);
        i.putExtra(getString(R.string.position_var), position);
        setResult(RESULT_OK, i);
        finish();
    }

    /**
     * Gets objects from view
     */
    private void initializeView() {
        title = findViewById(R.id.txt_edit_title);
        userRating = findViewById(R.id.txt_edit_user_rating);
        userRatingSlider = findViewById(R.id.seekBar_edit_user_rating);
        userRatingSlider.setMax(100);
        watched = findViewById(R.id.check_edit_watched);
        comment = findViewById(R.id.editTxt_edit_comment);
        cancel = findViewById(R.id.btn_edit_cancel);
        ok = findViewById(R.id.btn_edit_ok);
    }
}

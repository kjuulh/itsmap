package com.hermansendev.a01_tottenromatoes.services;

import android.util.JsonReader;
import android.util.JsonWriter;

import com.hermansendev.a01_tottenromatoes.models.Movie;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MoviesJSON {

    // I will not pass context, to get access to resoucces. I'd rather just hardcode the strings
    private static final String TITLE = "Title";
    private static final String PLOT = "Plot";
    private static final String GENRES = "Genres";
    private static final String RATING = "Rating";
    private static final String PICTURE = "Picture";
    private static final String COMMENT = "Comment";
    private static final String USER_RATING = "UserRating";
    private static final String WATCHED = "Watched";

    /**
     * Initialize writer to json, this is for multiple movies
     *
     * @param out
     * @param movies
     * @throws IOException
     */
    public void WriteMoviesJsonStream(OutputStream out, List<Movie> movies) throws IOException {
        // Creating a jsonwriter from stream
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out));
        writeMoviesArray(writer, movies);
        writer.close();
    }

    /**
     * Initialize writer to json, this is for a single movie
     *
     * @param out
     * @param movie
     * @throws IOException
     */
    public void WriteMovieJsonStream(OutputStream out, Movie movie) throws IOException {
        // Creating a jsonwriter from stream
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out));
        writeMovie(writer, movie);
        writer.close();
    }

    /**
     * Writer json array for movies
     *
     * @param writer
     * @param movies
     * @throws IOException
     */
    private void writeMoviesArray(JsonWriter writer, List<Movie> movies) throws IOException {
        writer.beginArray();
        for (Movie movie : movies) {
            writeMovie(writer, movie);
        }
        writer.endArray();
    }

    /**
     * Write json object for movie
     *
     * @param writer
     * @param movie
     * @throws IOException
     */
    private void writeMovie(JsonWriter writer, Movie movie) throws IOException {
        writer.beginObject();
        writer.name(TITLE).value(movie.getTitle());
        writer.name(PLOT).value(movie.getPlot());
        writer.name(GENRES);
        writeGenresArray(writer, movie.getGenres());
        writer.name(RATING).value(movie.getRating());
        writer.name(PICTURE).value(movie.getPicture());
        writer.name(COMMENT).value(movie.getComment());
        writer.name(USER_RATING).value(movie.getUserRating());
        writer.name(WATCHED).value(movie.isWatched());
        writer.endObject();
    }

    /**
     * Write array for genres
     *
     * @param writer
     * @param genres
     * @throws IOException
     */
    private void writeGenresArray(JsonWriter writer, ArrayList<String> genres) throws IOException {
        writer.beginArray();
        for (String genre : genres) {
            writer.value(genre);
        }
        writer.endArray();
    }

    /**
     * Json to object, for multiple movies
     *
     * @param in
     * @return
     * @throws IOException
     */
    public ArrayList<Movie> readMoviesJsonStream(InputStream in) throws IOException {
        // Creating a reader from stream
        JsonReader reader = new JsonReader(new InputStreamReader(in));
        try {
            return readMoviesArray(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Json to object, for a single movie
     *
     * @param in
     * @return
     * @throws IOException
     */
    public Movie readMovieJsonStream(InputStream in) throws IOException {
        // Creating a reader from stream
        JsonReader reader = new JsonReader(new InputStreamReader(in));
        try {
            return readMovie(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Read array of movies
     *
     * @param reader
     * @return
     * @throws IOException
     */
    private ArrayList<Movie> readMoviesArray(JsonReader reader) throws IOException {
        ArrayList<Movie> movies = new ArrayList<Movie>();

        reader.beginArray();
        while (reader.hasNext()) {
            movies.add(readMovie(reader));
        }
        reader.endArray();
        return movies;
    }

    /**
     * Read movie
     *
     * @param reader
     * @return
     * @throws IOException
     */
    private Movie readMovie(JsonReader reader) throws IOException {
        Movie movie = new Movie();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();

            switch (name) {
                case TITLE:
                    movie.setTitle(reader.nextString());
                    break;
                case PLOT:
                    movie.setPlot(reader.nextString());
                    break;
                case GENRES:
                    movie.setGenres(readGenres(reader));
                    break;
                case RATING:
                    movie.setRating((float) reader.nextDouble());
                    break;
                case PICTURE:
                    movie.setPicture(reader.nextString());
                    break;
                case COMMENT:
                    movie.setComment(reader.nextString());
                    break;
                case USER_RATING:
                    movie.setUserRating((float) reader.nextDouble());
                    break;
                case WATCHED:
                    movie.setWatched(reader.nextBoolean());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return movie;
    }

    /**
     * Read genres array
     *
     * @param reader
     * @return
     * @throws IOException
     */
    private ArrayList<String> readGenres(JsonReader reader) throws IOException {
        ArrayList<String> genres = new ArrayList<String>();

        reader.beginArray();
        while (reader.hasNext()) {
            genres.add(reader.nextString());
        }
        reader.endArray();
        return genres;
    }
}

package com.hermansendev.a01_tottenromatoes.repositories;

import com.hermansendev.a01_tottenromatoes.models.Movie;

import java.util.ArrayList;

interface IMovieRepository {
    void AddMovie(Movie movie);

    ArrayList<Movie> GetAll();

    Movie GetMovie(int position);

    void EditMovie(int position, Movie movie);

    void deleteMovie(int position);
}

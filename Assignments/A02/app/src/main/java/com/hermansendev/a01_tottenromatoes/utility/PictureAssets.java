package com.hermansendev.a01_tottenromatoes.utility;

import android.util.Log;

import com.hermansendev.a01_tottenromatoes.R;

public class PictureAssets {

    // Strings won't be externalized, as it is located in a subclass,
    // and should only be updated/used here
    private static final String ACTION = "Action";
    private static final String ADVENTURE = "Adventure";
    private static final String FANTASY = "Fantasy";
    private static final String SCI_FI = "Sci-Fi";
    private static final String BIOGRAPHY = "Biography";
    private static final String DRAMA = "Drama";
    private static final String MUSIC = "Music";
    private static final String ROMANCE = "Romance";
    private static final String COMEDY = "Comedy";
    private static final String ANIMATION = "Animation";
    private static final String FAMILY = "Family";
    private static final String THRILLER = "Thriller";
    private static final String SPORT = "Sport";
    private static final String HISTORY = "History";

    private static final String TAG = "PictureAssets";

    /**
     * Returns a picture asset id based on input genre.
     *
     * @param genre
     * @return
     */
    public static int getPicture(String genre) {
        Log.d(TAG, "getPicture: " + genre);

        switch (genre) {
            case ACTION:
                return R.drawable.action;
            case ADVENTURE:
                return R.drawable.adventure;
            case FANTASY:
                return R.drawable.fantasy;
            case SCI_FI:
                return R.drawable.sci_fi;
            case BIOGRAPHY:
                return R.drawable.biography;
            case DRAMA:
                return R.drawable.drama;
            case MUSIC:
                return R.drawable.music;
            case ROMANCE:
                return R.drawable.romance;
            case COMEDY:
                return R.drawable.comedy;
            case ANIMATION:
                return R.drawable.animation;
            case FAMILY:
                return R.drawable.family;
            case THRILLER:
                return R.drawable.thriller;
            case SPORT:
                return R.drawable.sport;
            case HISTORY:
                return R.drawable.history;
            default: {
                return R.drawable.history;
                //throw new IllegalArgumentException("Genre doesn't exists");
            }
        }
    }
}

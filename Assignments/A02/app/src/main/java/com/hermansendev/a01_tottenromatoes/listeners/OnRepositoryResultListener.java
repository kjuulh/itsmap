package com.hermansendev.a01_tottenromatoes.listeners;

public interface OnRepositoryResultListener<T> {
    void getResult(T items);
}

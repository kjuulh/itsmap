package com.hermansendev.a01_tottenromatoes;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hermansendev.a01_tottenromatoes.listeners.OnControllerUpdateListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.services.MoviesService;

import static android.support.v4.content.LocalBroadcastManager.getInstance;

public class EditActivity extends AppCompatActivity {

    private static final String TAG = "EditActivity";
    // Primary movie that the view interacts with
    Movie movie;

    TextView title;
    TextView userRating;
    SeekBar userRatingSlider;
    CheckBox watched;
    EditText comment;
    Button ok;
    Button cancel;

    private MoviesService moviesService;
    private boolean bound = false;
    private String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        if (savedInstanceState != null) {
            id = savedInstanceState.getString(getString(R.string.saved_id));
        } else {
            Intent i = getIntent();
            // Get class from intent
            id = i.getStringExtra(getString(R.string.resource_id));
        }

        // get objects from view
        initializeView();
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        updateMovie();
        super.onSaveInstanceState(bundle);
        bundle.putString(getString(R.string.saved_id), id);
    }

    /**
     * Provides a connection to the service
     */
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected: ");
            MoviesService.LocalBinder binder = (MoviesService.LocalBinder) service;
            moviesService = binder.getService();
            bound = true;

            handleId();
            // set listeners
            setOnClickListeners();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected: ");
            bound = false;
        }
    };

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();

        // Bind to service
        Intent intent = new Intent(this, MoviesService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
        unbindService(connection);
        bound = false;
    }

    /**
     * Set view information, based of movie
     */
    private void setEditInfo() {
        title.setText(movie.getTitle());
        // set userRating based of float precision 0.1
        userRating.setText(toFloat(movie.getUserRating()));
        userRatingSlider.setProgress((int) (movie.getUserRating() * 10));
        watched.setChecked(movie.isWatched());
        comment.setText(movie.getComment());
    }

    private String toFloat(float userRating) {
        return String.format("%.1f", userRating);
    }

    /**
     * Handle intent from another class
     */
    private void handleId() {
        moviesService.getMovieById(id, new OnControllerUpdateListener<Movie>() {
            @Override
            public void getResult(Movie items) {
                movie = items;
                moviesService.saveOldMovie(movie);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setEditInfo();
                    }
                });
            }
        });
    }

    /**
     * Set all listeners
     */
    private void setOnClickListeners() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOkClick();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancelClick();
            }
        });
        userRatingSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                handleProgress(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        watched.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                handleCheckWatched(isChecked);
            }
        });
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                handleEditText(s.toString());
            }
        });
    }

    private void updateMovie() {
        moviesService.updateMovie(movie, new OnControllerUpdateListener<Void>() {
            @Override
            public void getResult(Void items) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EditActivity.this, "Movie Updated", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    /**
     * Set user comment in movie
     *
     * @param text
     */
    private void handleEditText(String text) {
        movie.setComment(text);
    }

    /**
     * set watched status in movie
     *
     * @param isChecked
     */
    private void handleCheckWatched(boolean isChecked) {
        movie.setWatched(isChecked);
    }

    /**
     * Convert progress into userrating with precision 0.1
     *
     * @param progress
     */
    private void handleProgress(int progress) {
        Log.d(TAG, "handleProgress: " + progress);
        Float value = progress * 0.1f;
        movie.setUserRating(value);
        userRating.setText(toFloat(value));
    }

    /**
     * Returns a cancelled result to previous activity
     */
    private void handleCancelClick() {
        if (moviesService != null) {
            movie = moviesService.getOldMovie();
            moviesService.clearOldMovie();
            updateMovie();
        }

        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * Persists movie object and position, then returns OK result
     */
    private void handleOkClick() {
        updateMovie();
        if (moviesService != null) {
            moviesService.clearOldMovie();
        }
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        finish();
    }

    /**
     * Gets objects from view
     */
    private void initializeView() {
        title = findViewById(R.id.txt_edit_title);
        userRating = findViewById(R.id.txt_edit_user_rating);
        userRatingSlider = findViewById(R.id.seekBar_edit_user_rating);
        userRatingSlider.setMax(100);
        watched = findViewById(R.id.check_edit_watched);
        comment = findViewById(R.id.editTxt_edit_comment);
        cancel = findViewById(R.id.btn_edit_cancel);
        ok = findViewById(R.id.btn_edit_ok);
    }
}

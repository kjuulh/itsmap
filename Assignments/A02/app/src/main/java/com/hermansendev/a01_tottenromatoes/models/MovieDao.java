package com.hermansendev.a01_tottenromatoes.models;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM Movies")
    List<Movie> GetAll();

    @Query("SELECT * FROM Movies WHERE id LIKE :id LIMIT 1")
    Movie findById(String id);

    @Query("SELECT * FROM Movies WHERE title LIKE :title LIMIT 1")
    Movie findByTitle(String title);

    @Insert
    void insert(Movie... movies);

    @Update
    void update(Movie... movies);

    @Delete
    void delete(Movie movie);
}

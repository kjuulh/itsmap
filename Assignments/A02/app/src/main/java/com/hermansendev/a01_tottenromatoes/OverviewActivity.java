package com.hermansendev.a01_tottenromatoes;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.hermansendev.a01_tottenromatoes.listeners.OnControllerUpdateListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.services.MoviesService;

import java.util.ArrayList;
import java.util.List;

public class OverviewActivity extends AppCompatActivity {

    private static final String TAG = "OverviewActivity";
    private ListView moviesListView;
    private MoviesAdapter moviesAdapter;

    private ArrayList<Movie> movies = new ArrayList<Movie>();

    private MoviesService moviesService;
    private boolean bound = false;

    Button exitBtn;
    Button addBtn;
    EditText movieTitleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        Stetho.initializeWithDefaults(this);

        // Get widgets from view
        initializeWidgets();

        // Sets listeners
        setOnClickListeners();

        Intent intent = new Intent(this, MoviesService.class);
        startService(intent);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();

        // Bind to service
        Intent intent = new Intent(this, MoviesService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop(){
        Log.d(TAG, "onStop: ");
        super.onStop();
        unbindService(connection);
        bound = false;
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected: ");
            MoviesService.LocalBinder binder = (MoviesService.LocalBinder) service;
            moviesService = binder.getService();
            bound = true;

            // Get movies
            InitializeMovies();

            class MyBroadcastReceiver extends BroadcastReceiver {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(TAG, "onReceive: MyBroadCastReceiver");
                    InitializeMovies();
                }
            }
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            filter.addAction("com.hermansendev.a01_tottenromatoes.ADDED_MOVIE");

            LocalBroadcastManager.getInstance(OverviewActivity.this).registerReceiver(new MyBroadcastReceiver(), filter);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected: ");
            bound = false;
        }
    };

    /**
     * Gets widgets to handle in code
     */
    private void initializeWidgets() {
        Log.d(TAG, "initializeWidgets: ");
        addBtn = findViewById(R.id.btn_overview_add);
        exitBtn = findViewById(R.id.btn_overview_exit);
        movieTitleText = findViewById(R.id.edit_overview_title);

        moviesAdapter = new MoviesAdapter(this, new ArrayList<Movie>());
        moviesListView = findViewById(R.id.list_overview_movies);
        moviesListView.setAdapter(moviesAdapter);
    }

    private void InitializeMovies() {
        Log.d(TAG, "InitializeMovies: ");
        // get adapter and set movies
        moviesService.getAllMovies(new OnControllerUpdateListener<List<Movie>>() {
            @Override
            public void getResult(List<Movie> items) {
                handleServiceResult(items);
            }
        });
    }

    private void handleServiceResult(final List<Movie> items) {
        AddMoviesToAdapter(items);
    }

    private void AddMoviesToAdapter(final List<Movie> items) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moviesAdapter.movieList = items;
                moviesAdapter.notifyDataSetChanged();
                moviesListView.setSelection(moviesAdapter.getCount() - 1);
            }
        });
    }

    private void setOnClickListeners() {
        Log.d(TAG, "setOnClickListeners: ");
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleExit();
            }
        });
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                handleAdd();
            }
        });
        moviesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                handleNormalClick(parent, view, position, id);
            }
        });
        moviesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return handleLongClick(parent, view, position, id);
            }
        });
    }

    private void handleAdd() {
        Log.d(TAG, "handleAdd: Clicked");
        if (bound) {
            moviesService.addMovie(movieTitleText.getText().toString(), new OnControllerUpdateListener<Void>() {
                @Override
                public void getResult(Void items) {
                    Log.d(TAG, "OverviewActivity: Returned to main");
                    // I originally implemented listeners, until I found out that Broadcast Receivers were a requirement
                    // Broadcast receivers are only implemented for add movie callback, the rest uses listeners (observer pattern)
                }
            });
        }
    }

    /**
     * Exits the process (there might be a more graceful method)
     */
    private void handleExit() {
        Log.d(TAG, "handleExit: ");
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    /**
     * Handle a normal click on a movie (detail view)
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    private void handleNormalClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "handleNormalClick: ");

        //unbindService(connection);
        //bound = false;
        // get intent for detailview
        Intent startDetailIntent = new Intent();
        // get position
        startDetailIntent.putExtra(getString(R.string.resource_id), ((Movie)moviesAdapter.getItem(position)).getMovieId());

        // add position and add to intent
        //movies.get(position).AddToIntent(startDetailIntent, this);
        String action = getString(R.string.action_details);
        int resultCode = getResources().getInteger(R.integer.request_details);

        // set action and start activity
        if (action != null && !action.equals("")){
            startDetailIntent.setAction(action);
            startActivityForResult(startDetailIntent, resultCode);
        }
    }

    /**
     * handle a looong click on a movie (edit view)
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    private boolean handleLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "handleLongClick: ");
        // get intent for editview
        Intent startEditIntent = new Intent();

        //unbindService(connection);
        //bound = false;
        // get position
        startEditIntent.putExtra(getString(R.string.resource_id), ((Movie)moviesAdapter.getItem(position)).getMovieId());

        // Put movie position
        //movies.get(position).AddToIntent(startEditIntent, this);
        String action = getString(R.string.action_edit);
        int resultCode = getResources().getInteger(R.integer.request_edit);

        // Start activity
        if (action != null && !action.equals("")){
            startEditIntent.setAction(action);
            startActivityForResult(startEditIntent, resultCode);
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult: ");

        // From where did we return
        if (requestCode == getInteger(R.integer.request_details)){ // handle origin detailview
            switch (resultCode){
                case RESULT_OK: // if okay
                    handleDetailsReturnSuccess(data);
                    break;
                case RESULT_CANCELED:
                    handleDetailsReturnCancelled(data);
            }
        } else if(requestCode == getInteger(R.integer.request_edit)) { // handle origin editview
            switch (resultCode){
                case RESULT_OK: // if okay
                    handleEditReturnSuccess(data);
                    break;
                case RESULT_CANCELED:
                    handleEditReturnCancelled(data);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleDetailsReturnSuccess(Intent data) {
        Log.d(TAG, "handleDetailsReturnSuccess: ");
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleDetailsReturnCancelled(Intent data) {
        Log.d(TAG, "handleDetailsReturnCancelled: ");
    }

    /**
     * gets movie from edit activity and updates view
     * @param data
     */
    private void handleEditReturnSuccess(Intent data) {
        Log.d(TAG, "handleEditReturnSuccess: ");
    }

    /**
     * Redundant for now
     * @param data
     */
    private void handleEditReturnCancelled(Intent data) {
        Log.d(TAG, "handleEditReturnCancelled: ");
    }

    // returns integer based of resources
    private int getInteger(int resId) {
        return getResources().getInteger(resId);
    }
}

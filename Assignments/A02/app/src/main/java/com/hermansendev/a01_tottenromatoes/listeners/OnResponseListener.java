package com.hermansendev.a01_tottenromatoes.listeners;

public interface OnResponseListener<T> {
    void getResult(T object);
}

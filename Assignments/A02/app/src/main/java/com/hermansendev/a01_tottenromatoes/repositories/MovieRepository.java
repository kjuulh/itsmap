package com.hermansendev.a01_tottenromatoes.repositories;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.hermansendev.a01_tottenromatoes.listeners.OnRepositoryResultListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.models.database.AppDatabase;

import java.util.List;

public class MovieRepository {

    private AppDatabase appDatabase;
    private String TAG = "MovieRepository";

    public MovieRepository(Context context) {
        appDatabase = AppDatabase.getInstance(context);
    }

    /**
     * Add a movie to the db
     *
     * @param movie
     */
    public void add(final Movie movie) {
        class AddTask extends AsyncTask<Movie, Void, Void> {

            @Override
            protected Void doInBackground(Movie... movies) {
                try {
                    appDatabase.movieDao().insert(movies);
                } catch (Exception e){
                    Log.d(TAG, "doInBackground: Failed to add movie " + movies[0].getTitle());
                }
                return null;
            }
        }

        if (movie != null){
            new AddTask().execute(movie);
        } else {
            Log.d(TAG, "add: Movie is null");
        }
    }

    /**
     * Returns a clone of the object, this is to emulate an enumerable.
     *
     * @return
     */
    public void getAll(final OnRepositoryResultListener<List<Movie>> listener) {
        class GetAllTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                listener.getResult(appDatabase.movieDao().GetAll());
                return null;
            }
        }

        new GetAllTask().execute();
    }

    /**
     * Get movie from index
     *
     * @param id
     * @return
     */
    public void getById(String id, final OnRepositoryResultListener<Movie> listener) {
        class GetTask extends AsyncTask<String, Void, Void> {
            @Override
            protected Void doInBackground(String... id) {
                Movie movie = appDatabase.movieDao().findById(id[0]);
                if (movie != null) {
                    listener.getResult(movie);
                }
                return null;
            }
        }

        new GetTask().execute(id);
    }

    public void getByTitle(String title, final OnRepositoryResultListener<Movie> listener) {

        class GetTask extends AsyncTask<String, Void, Void> {
            @Override
            protected Void doInBackground(String... title) {
                listener.getResult(appDatabase.movieDao().findByTitle(title[0]));
                return null;
            }
        }

        new GetTask().execute(title);
    }

    /**
     * Update a movie
     *
     * @param movie
     */
    public void update(Movie movie, final OnRepositoryResultListener<Void> listener) {
        class UpdateTask extends AsyncTask<Movie, Void, Void> {
            @Override
            protected Void doInBackground(Movie... movies) {
                appDatabase.movieDao().update(movies[0]);
                listener.getResult(null);
                return null;
            }
        }

        new UpdateTask().execute(movie);
    }

    /**
     * Delete a movie at index
     *
     * @param id
     */
    public void delete(String id, final OnRepositoryResultListener<Void> listener) {

        class DeleteTask extends AsyncTask<String, Void, Void> {
            @Override
            protected Void doInBackground(String... id) {
                getById(id[0],
                        new OnRepositoryResultListener<Movie>() {
                            @Override
                            public void getResult(Movie items) {
                                appDatabase.movieDao().delete(items);
                                listener.getResult(null);
                            }
                        });

                return null;
            }
        }

        new DeleteTask().execute(id);
    }
}

package com.hermansendev.a01_tottenromatoes.providers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.hermansendev.a01_tottenromatoes.R;
import com.hermansendev.a01_tottenromatoes.listeners.OnCsvMoviesLoadListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVMoviesProvider {
    private String TAG = "CSVMoviesProvider";

    public void getMovieTitles(final Context context, final OnCsvMoviesLoadListener<List<String>> listener) {
        class GetMoviesTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                // Create reader for moviesCsvList
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                context.getResources().openRawResource(R.raw.movielist)
                        )
                );
                ArrayList<String[]> resultList = new ArrayList<>();
                try {
                    String csvLine;

                    while ((csvLine = reader.readLine()) != null) {
                        resultList.add(csvLine.split(";"));
                    }
                } catch (Exception e) {
                    Log.d(TAG, "doInBackground: Loading of csv movies failed");
                } finally {
                    try {
                        // Cleanup
                        reader.close();
                    } catch (IOException e) {
                        Log.d(TAG, "doInBackground: Couldn't free resources");
                    }
                }
                // remove heading
                resultList.remove(0);

                // Only titles are needed, the rest are pulled from api
                ArrayList<String> titles = new ArrayList<>();
                for (String[] movie :
                        resultList) {
                    titles.add(movie[0]);
                }

                // Returns callback
                listener.getResult(titles);

                return null;
            }
        }

        // Execute async
        new GetMoviesTask().execute();
    }
}

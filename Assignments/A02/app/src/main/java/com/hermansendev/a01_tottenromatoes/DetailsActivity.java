package com.hermansendev.a01_tottenromatoes;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.hermansendev.a01_tottenromatoes.listeners.OnControllerUpdateListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.services.MoviesService;
import com.hermansendev.a01_tottenromatoes.utility.PictureAssets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = "DetailsActivity";
    // Primary movie, that the view is based of
    Movie movie;

    ImageView image;
    TextView title;
    TextView rating;
    TextView userRating;
    TextView plot;
    TextView comment;
    TextView genres;
    CheckBox watched;
    Button okButton;
    Button deleteButton;
    private MoviesService moviesService;
    private boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        initializeView();
        setOnClickListeners();
    }

    /**
     * Provides a connection to the service
     */
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected: OverviewActivity");
            MoviesService.LocalBinder binder = (MoviesService.LocalBinder) service;
            moviesService = binder.getService();
            bound = true;

            handleIntent();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected: OverviewActivity");
            bound = false;
        }
    };

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();

        // Bind to service
        Intent intent = new Intent(this, MoviesService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
        unbindService(connection);
        bound = false;
    }

    /**
     * Handle intent sent from another activity, and extracts movie object
     */
    private void handleIntent() {
        // Get intent from other activity
        Intent i = getIntent();
        // Get class from intent
        String id = i.getStringExtra(getString(R.string.resource_id));
        moviesService.getMovieById(id, new OnControllerUpdateListener<Movie>() {
            @Override
            public void getResult(Movie items) {
                movie = items;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setDetailsInfo();
                    }
                });
            }
        });
    }

    /**
     * Fill view with information
     */
    private void setDetailsInfo() {

        // Get asset based on primary genre
        Drawable imageDrawable = this.getResources().getDrawable(
                PictureAssets.getPicture(
                        movie.getPrimaryGenre()
                ));
        image.setImageDrawable(imageDrawable);

        title.setText(movie.getTitle());
        rating.setText(floatToString(movie.getRating()));
        userRating.setText(floatToString(movie.getUserRating()));
        plot.setText(movie.getPlot());
        comment.setText(movie.getComment());
        genres.setText(movie.getGenres());
        watched.setChecked(movie.isWatched());
    }

    private String floatToString(float rating) {
        // set precision on user specified ratings, this should mitigate float inconsistencies
        return String.format("%.1f", rating);
    }

    /**
     * Initialize button(s)
     */
    private void setOnClickListeners() {
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOk();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDelete();
            }
        });
    }

    private void handleDelete() {
        moviesService.removeMovie(movie.getMovieId(), new OnControllerUpdateListener<Void>() {
            @Override
            public void getResult(Void items) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        handleOk();
                    }
                });
            }
        });
    }

    /**
     * Return to the previous activity with rc OK
     */
    private void handleOk() {
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Get objects in view
     */
    private void initializeView() {
        image = findViewById(R.id.img_details_image);
        title = findViewById(R.id.txt_details_title);
        rating = findViewById(R.id.txt_details_rating);
        userRating = findViewById(R.id.txt_details_user_rating);
        plot = findViewById(R.id.txt_details_plot);
        comment = findViewById(R.id.txt_details_user_comment);
        genres = findViewById(R.id.txt_details_genres);
        watched = findViewById(R.id.check_details_watched);
        okButton = findViewById(R.id.btn_details_ok);
        deleteButton = findViewById(R.id.btn_details_delete);
    }
}

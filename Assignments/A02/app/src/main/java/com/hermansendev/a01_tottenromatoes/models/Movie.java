package com.hermansendev.a01_tottenromatoes.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(
        tableName = "Movies",
        indices =  {@Index(value = {"title"}, unique = true)}
)
public class Movie {

    public Movie(
            String movieId,
            String title,
            String plot,
            String genres,
            float rating
    ){
        setMovieId(movieId);
        setTitle(title);
        setPlot(plot);
        setGenres(genres);
        setRating(rating);
    }

    @PrimaryKey()
    @ColumnInfo(name = "id")
    @NonNull
    private String movieId = "";

    @ColumnInfo(name = "title")
    private String title = "";

    @ColumnInfo(name="plot")
    private String plot = "";

    @ColumnInfo(name="genres")
    private String genres = "";

    @ColumnInfo(name="rating")
    private float rating = 0.0f;

    // User
    @ColumnInfo(name="user_comment")
    private String comment = "";

    @ColumnInfo(name="user_rating")
    private float userRating = 5.0f;

    @ColumnInfo(name="user_status")
    private boolean watched = false;

    public boolean isWatched() {
        return watched;
    }

    public void setWatched(boolean watched) {
        this.watched = watched;
    }

    public float getUserRating() {
        return userRating;
    }

    public void setUserRating(float userRating) {
        this.userRating = userRating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres( String genres) {
        this.genres = genres;
    }

    public String getPrimaryGenre() {
        return getGenres().split(", ")[0];
    }
}
package com.hermansendev.a01_tottenromatoes.providers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.GsonBuildConfig;
import com.hermansendev.a01_tottenromatoes.R;
import com.hermansendev.a01_tottenromatoes.listeners.OnErrorListener;
import com.hermansendev.a01_tottenromatoes.listeners.OnResponseListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MovieProvider {

    private static final String TAG = "MovieProvider";
    private Context context;
    private String apiKey;
    private String baseUrl;

    public MovieProvider(Context context) {
        this.context = context;
        apiKey = context.getString(R.string.apiKey);
        baseUrl = context.getString(R.string.baseUrl);
    }

    public void getMovie(String title, final OnResponseListener<Movie> responseListener, final OnErrorListener errorListener) {
        // Http Request
        requestMovie(title, responseListener, errorListener);
    }

    private APIMovie convertJson(String json) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        // Convert
        return gson.fromJson(json, APIMovie.class);
    }

    private final List<Movie> movies = new ArrayList<>();

    private void requestMovie(String title, final OnResponseListener<Movie> responseListener, final OnErrorListener errorListener) throws NullPointerException {

        // Make sure title is not null or unusable
        if ((title == null) || (title.equals(""))) {
            throw new NullPointerException();
        }

        // Clean title for whitespace (not a perfect solution)
        title = title.replace(" ", "+");

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getUrl(title), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Handle response, and use callback
                        responseListener.getResult(handleResponse(response.toString()));

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleError(error);
                        errorListener.getError();
                    }
                });

        // Fire request
        queue.add(jsonObjectRequest);
    }

    /**
     * Construct url for title
     *
     * @param title
     * @return
     */
    private String getUrl(String title) {
        return baseUrl + "?t=" + title + "&apikey=" + apiKey;
    }

    private void handleError(VolleyError error) {
        Log.d(TAG, "handleError: " + error.toString());
    }

    private Movie handleResponse(String response) {
        Log.d(TAG, "handleResponse: Received");

        // Gson json -> APIMovie
        APIMovie apiMovie;
        try {
            apiMovie = convertJson(response);

            // Map APIMovie -> Movie
            Movie movie = new Movie(
                    apiMovie.getImdbID(),
                    apiMovie.getTitle(),
                    apiMovie.getPlot(),
                    apiMovie.getGenre(),
                    Float.parseFloat(apiMovie.getImdbRating())
            );

            return movie;
        } catch (Exception e) {
            Log.d(TAG, "handleResponse: Movie couldn't be parsed");
            return null;
        }
    }

    private class APIMovie {
        @SerializedName(value = "title", alternate = {"Title"})
        private String title;
        @SerializedName(value = "rated", alternate = {"Rated"})
        private String rated;
        @SerializedName(value = "released", alternate = {"Released"})
        private String released;
        @SerializedName(value = "genre", alternate = {"Genre"})
        private String genre;
        @SerializedName(value = "plot", alternate = {"Plot"})
        private String plot;
        @SerializedName(value = "poster", alternate = {"Poster"})
        private String poster;
        @SerializedName(value = "imdbRating", alternate = {"ImdbRating", "IMDBRating"})
        private String imdbRating;
        @SerializedName(value = "imdbID", alternate = {"ImdbID", "IMDBID"})
        private String imdbID;
        @SerializedName(value = "type", alternate = {"Type"})
        private String type;

        public String getTitle() {
            return title;
        }

        public void setTitle(String value) {
            this.title = value;
        }

        public String getRated() {
            return rated;
        }

        public void setRated(String value) {
            this.rated = value;
        }

        public String getReleased() {
            return released;
        }

        public void setReleased(String value) {
            this.released = value;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String value) {
            this.genre = value;
        }

        public String getPlot() {
            return plot;
        }

        public void setPlot(String value) {
            this.plot = value;
        }

        public String getPoster() {
            return poster;
        }

        public void setPoster(String value) {
            this.poster = value;
        }

        public String getImdbRating() {
            return imdbRating;
        }

        public void setImdbRating(String value) {
            this.imdbRating = value;
        }

        public String getImdbID() {
            return imdbID;
        }

        public void setImdbID(String value) {
            this.imdbID = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String value) {
            this.type = value;
        }
    }

}

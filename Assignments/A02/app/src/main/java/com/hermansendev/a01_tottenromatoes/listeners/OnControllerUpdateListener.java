package com.hermansendev.a01_tottenromatoes.listeners;

public interface OnControllerUpdateListener<T> {
    void getResult(T items);
}

package com.hermansendev.a01_tottenromatoes.listeners;

public interface OnErrorListener {
    void getError();
}

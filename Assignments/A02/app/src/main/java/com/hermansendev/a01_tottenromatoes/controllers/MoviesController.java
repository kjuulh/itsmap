package com.hermansendev.a01_tottenromatoes.controllers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hermansendev.a01_tottenromatoes.listeners.OnControllerUpdateListener;
import com.hermansendev.a01_tottenromatoes.listeners.OnErrorListener;
import com.hermansendev.a01_tottenromatoes.listeners.OnRepositoryResultListener;
import com.hermansendev.a01_tottenromatoes.listeners.OnResponseListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.providers.MovieProvider;
import com.hermansendev.a01_tottenromatoes.repositories.MovieRepository;

import java.util.List;

public class MoviesController {

    private final static String TAG = "MoviesController";

    private static MoviesController instance;

    private MoviesController(Context context) {
        movieRepository = new MovieRepository(context);
        movieProvider = new MovieProvider(context);
    }

    public static MoviesController getInstance(Context context) {
        if (instance == null) {
            instance = new MoviesController(context);
        }

        return instance;
    }

    private MovieRepository movieRepository;
    private MovieProvider movieProvider;

    public void ping() {
        Log.d(TAG, "ping: Pong!");
    }

    public void addMovie(final Context context, final String title, final OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "addMovie: Called");
        movieRepository.getByTitle(title, new OnRepositoryResultListener<Movie>() {
            @Override
            public void getResult(Movie items) {
                if (items == null) {
                    Log.d(TAG, "addMovie: Movie wasn't found in db, fetching");
                    try {
                        movieProvider.getMovie(
                                title,
                                new OnResponseListener<Movie>() {
                                    @Override
                                    public void getResult(Movie movie) {
                                        if (movie == null) {
                                            Log.d(TAG, "addMovie: Movie wasn't found in api, returning with error");
                                        } else {
                                            movieRepository.add(movie);
                                            Log.d(TAG, "addMovie: Movie was saved at title:" + movie.getTitle());
                                            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("com.hermansendev.a01_tottenromatoes.ADDED_MOVIE"));
                                        }
                                        listener.getResult(null);
                                    }
                                },
                                new OnErrorListener() {
                                    @Override
                                    public void getError() {
                                        Log.d(TAG, "getError: Received a http error");
                                    }
                                });
                    } catch (Exception e) {
                        Log.d(TAG, "getResult: Failed to parse title");
                    }
                } else {
                    Log.d(TAG, "addMovie: Movie was found in db already");
                }
            }
        });
    }

    public void getAllMovies(final OnControllerUpdateListener<List<Movie>> listener) {
        Log.d(TAG, "getAllMovies: Called");
        movieRepository.getAll(
                new OnRepositoryResultListener<List<Movie>>() {
                    @Override
                    public void getResult(List<Movie> items) {
                        Log.d(TAG, "getResult: " + items.size());
                        listener.getResult(items);
                    }
                });
    }

    public void getMovieByTitle(String title, final OnControllerUpdateListener<Movie> listener) {
        Log.d(TAG, "getMovieByTitle: Called");
        movieRepository.getByTitle(title, new OnRepositoryResultListener<Movie>() {
            @Override
            public void getResult(Movie items) {
                listener.getResult(items);
            }
        });
    }

    public void getMovieById(String id, final OnControllerUpdateListener<Movie> listener) {
        Log.d(TAG, "getMovieByTitle: Called");
        movieRepository.getById(id, new OnRepositoryResultListener<Movie>() {
            @Override
            public void getResult(Movie items) {
                listener.getResult(items);
            }
        });
    }

    public void updateMovie(Movie movie, final OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "updateMovie: Called");
        movieRepository.update(movie, new OnRepositoryResultListener<Void>() {
            @Override
            public void getResult(Void items) {
                listener.getResult(null);
            }
        });
    }

    public void deleteMovie(String id, final OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "deleteMovie: Called");
        movieRepository.delete(id, new OnRepositoryResultListener<Void>() {
            @Override
            public void getResult(Void items) {
                listener.getResult(null);
            }
        });
    }
}

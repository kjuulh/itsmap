package com.hermansendev.a01_tottenromatoes.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.hermansendev.a01_tottenromatoes.OverviewActivity;
import com.hermansendev.a01_tottenromatoes.R;
import com.hermansendev.a01_tottenromatoes.controllers.MoviesController;
import com.hermansendev.a01_tottenromatoes.listeners.OnControllerUpdateListener;
import com.hermansendev.a01_tottenromatoes.listeners.OnCsvMoviesLoadListener;
import com.hermansendev.a01_tottenromatoes.models.Movie;
import com.hermansendev.a01_tottenromatoes.providers.CSVMoviesProvider;
import com.hermansendev.a01_tottenromatoes.utility.PictureAssets;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MoviesService extends Service {

    private MoviesController controller;

    private static final String TAG = "MoviesService";
    private final IBinder binder = new LocalBinder();
    private Movie oldMovie;

    public MoviesService() {
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        Looper serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }

    private ServiceHandler serviceHandler;

    /**
     * Handler that receives messages from the thread
     */
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                while (true) {
                    Log.d(TAG, "handleMessage: " + new Date().toString());
                    Thread.sleep(2000 * 60); // 2 min
                    sendNotification();
                }
            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: Starting started service (Long running)");

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        serviceHandler.sendMessage(msg);

        return START_STICKY;
    }


    public class LocalBinder extends Binder {
        public MoviesService getService() {
            return MoviesService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: Binding service");
        controller = MoviesController.getInstance(getApplicationContext());
        loadMoviesFromCsv();
        return binder;
    }

    /**
     * Send notification
     */
    public void sendNotification() {
        Log.d(TAG, "sendNotification: Called");
        // Intent makes it possible to point to an Acitivity
        Intent notificationIntent = new Intent(getApplicationContext(), OverviewActivity.class);

        final PendingIntent pendingIntent =
                PendingIntent.getActivity(getApplicationContext(),
                        R.integer.notification_request_code,
                        notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Handle for specific version and Create channel for publishing notifications
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getString(R.string.notification_channel_id),
                    getString(R.string.name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(getString(R.string.notification_channel_description));
            manager.createNotificationChannel(channel);
        }

        // Get movies that haven't been watched
        getMoviesThatHaventBeenWatched(new OnControllerUpdateListener<List<Movie>>() {
            @Override
            public void getResult(List<Movie> items) {
                // If non-empty
                if (items.size() != 0) {
                    // Get random movie
                    Movie movie = items.get(new Random().nextInt(items.size()));

                    // Create notification
                    Notification notification = new NotificationCompat.Builder(
                            getApplicationContext(),
                            getString(R.string.notification_channel_id))
                            .setContentTitle(getString(R.string.notification_intro)
                                    + movie.getTitle()
                                    + getString(R.string.notification_end))
                            .setContentText(getString(R.string.notification_sub))
                            .setSmallIcon(R.drawable.ic_totten_romatoes_logo)
                            .setContentIntent(pendingIntent)
                            .build();

                    manager.notify(R.integer.notification_id, notification);
                    Log.d(TAG, "SendNotification: Sending Notification");
                }
            }
        });
    }

    /**
     * Simple test method to varify that the service is alive
     */
    public void ping() {
        Log.d(TAG, "sayHello: Pong!");
        controller.ping();
    }

    /**
     * Save a movie in the service, this will not persist in database
     *
     * @param movie
     */
    public void saveOldMovie(Movie movie) {
        Log.d(TAG, "saveOldMovie: ");
        if (this.oldMovie == null) {
            this.oldMovie = movie;
        } else {
            Log.d(TAG, "saveOldMovie: Movie is null");
        }
    }

    /**
     * Retrieves the movie stored in service, this can replace an updated version in the database
     *
     * @return
     */
    public Movie getOldMovie() {
        Log.d(TAG, "getOldMovie: ");
        return this.oldMovie;
    }

    /**
     * Clear cache of persisted movie
     */
    public void clearOldMovie() {
        Log.d(TAG, "clearOldMovie: ");
        this.oldMovie = null;
    }

    /**
     * Add a movie from the title, this will in the best case add a movie from api to the db
     *
     * @param title
     * @param listener
     */
    public void addMovie(String title, final OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "addMovie: Adding: " + title);
        try {
            controller.addMovie(getApplicationContext(), title, listener);
        } catch (Exception e) {
            Log.d(TAG, "addMovie: Failed to add: " + title);
        }
    }

    /**
     * Remove movie at id
     *
     * @param id
     * @param listener
     */
    public void removeMovie(String id, OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "removeMovie: " + id);
        controller.deleteMovie(id, listener);
    }

    /**
     * Update a movie
     *
     * @param movie
     * @param listener
     */
    public void updateMovie(Movie movie, OnControllerUpdateListener<Void> listener) {
        Log.d(TAG, "updateMovie: " + movie.getTitle());
        controller.updateMovie(movie, listener);
    }

    /**
     * Returns movie that match title
     *
     * @param title
     * @param listener
     */
    public void getMovieByTitle(String title, OnControllerUpdateListener<Movie> listener) {
        Log.d(TAG, "getMovieByTitle: " + title);
        controller.getMovieByTitle(title, listener);
    }

    /**
     * Returns movie that match id
     *
     * @param id
     * @param listener
     */
    public void getMovieById(String id, OnControllerUpdateListener<Movie> listener) {
        Log.d(TAG, "getMovieByTitle: " + id);
        controller.getMovieById(id, listener);
    }

    /**
     * Returns all movies in db
     *
     * @param listener
     */
    public void getAllMovies(OnControllerUpdateListener<List<Movie>> listener) {
        Log.d(TAG, "getAllMovies: ");
        controller.getAllMovies(listener);
    }

    /**
     * Returns movies that match a certain criteria, haven't been watched
     *
     * @param listener
     */
    public void getMoviesThatHaventBeenWatched(final OnControllerUpdateListener<List<Movie>> listener) {
        Log.d(TAG, "getMoviesThatHaventBeenWatched: ");
        controller.getAllMovies(new OnControllerUpdateListener<List<Movie>>() {
            @Override
            public void getResult(List<Movie> items) {
                List<Movie> movies = new ArrayList<Movie>();
                for (Movie movie :
                        items) {
                    if (!movie.isWatched()) {
                        movies.add(movie);
                    }
                }
                listener.getResult(movies);
            }
        });
    }

    /**
     * Using csvprovider, handle callback
     */
    private void loadMoviesFromCsv() {
        new CSVMoviesProvider().getMovieTitles(getApplicationContext(), new OnCsvMoviesLoadListener<List<String>>() {
            @Override
            public void getResult(List<String> items) {
                for (String title :
                        items) {
                    addMovie(title, new OnControllerUpdateListener<Void>() {
                        @Override
                        public void getResult(Void items) {
                            Log.d(TAG, "getResult: Added Movie");
                            // Do nothing for callback
                        }
                    });
                }
            }
        });
    }
}

package com.hermansendev.a01_tottenromatoes.listeners;

public interface OnCsvMoviesLoadListener<T> {
    void getResult(T items);
}

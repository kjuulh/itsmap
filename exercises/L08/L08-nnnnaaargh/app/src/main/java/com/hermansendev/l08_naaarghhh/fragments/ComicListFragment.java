package com.hermansendev.l08_naaarghhh.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hermansendev.l08_naaarghhh.R;
import com.hermansendev.l08_naaarghhh.models.Comic.Comic;

import java.util.List;

public class ComicListFragment extends Fragment {
    private ListView comicListView;
    private ComicRecyclerViewAdapter adapter;
    private List<Comic> comics;

    private ComicSelectorInterface comicSelector;

    public ComicListFragment() {}

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_comic_list, container, false);

        comicListView = (ListView) view.findViewById(R.id.listView);
        imgHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(movieSelector!=null){
                    movieSelector.viewSpecial();
                }
            }
        });
        updateMovies();

        return view;
    }

}

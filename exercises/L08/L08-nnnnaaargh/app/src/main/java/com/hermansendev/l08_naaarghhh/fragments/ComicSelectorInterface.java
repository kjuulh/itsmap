package com.hermansendev.l08_naaarghhh.fragments;

import com.hermansendev.l08_naaarghhh.models.Comic.Comic;

import java.util.List;

public interface ComicSelectorInterface {
    public void onComicSelected(int position);
    public List<Comic> getComicList();
    public Comic getCurrentSelection();
    public void viewSpecial();
}

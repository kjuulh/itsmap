package com.hermansendev.l08_naaarghhh;

import android.content.res.Configuration;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.hermansendev.l08_naaarghhh.models.Comic.Comic;
import com.hermansendev.l08_naaarghhh.models.Database.AppDatabase;
import com.hermansendev.l08_naaarghhh.models.Database.SeedDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppDatabase database;

    public enum  PhoneMode {
        PORTRAIT,
        LANDSCAPE
    }

    public enum UserMode {
        LIST_VIEW,
        DETAIL_VIEW
    }

    private PhoneMode phoneMode;
    private UserMode userMode;

    private static final String LIST_FRAG = "list_fragment";
    private static final String DETAILS_FRAG = "details_fragment";

    private ComicListFragment comicList;
    private ComicDetailsFragment comicDetails;

    private ConstraintLayout listContainer;
    private ConstraintLayout detailsContainer;

    private int selectedComicIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = AppDatabase.getInstance(this);
        SeedDatabase seedDatabase = new SeedDatabase(database);

        if (database.comicDao().GetAll().size() == 0) {
            seedDatabase.clear();
            seedDatabase.addComics();
            seedDatabase.addChapters();
            seedDatabase.addPages();
        }

        listContainer = (ConstraintLayout)findViewById(R.id.constraint_main_list);
        detailsContainer = (ConstraintLayout) findViewById(R.id.constraint_main_detail);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            phoneMode = PhoneMode.PORTRAIT;
        } else {
            phoneMode = PhoneMode.LANDSCAPE;
        }

        if (savedInstanceState == null) {
            selectedComicIndex = 0;
            userMode = UserMode.LIST_VIEW;

            comicList = new ComicListFragment();
            comicDetails= new ComicDetailsFragment();

            List<Comic> comics = database.comicDao().GetAll();
            comicList.setComics(comics);
            comicDetails.setComic(database.comicDao().findById(comics.get(0).getComicId()));

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.constraint_main_list, comicList, LIST_FRAG)
                    .replace(R.id.constraint_main_detail, comicDetails, DETAILS_FRAG)
                    .commit();
        } else {
            selectedComicIndex = savedInstanceState.getInt("comic_position");

            userMode = (UserMode)savedInstanceState.getSerializable("user_mode");

            if (userMode == null){
                userMode = UserMode.LIST_VIEW;
            }

            comicList (ComicListFragment)getSupportFragmentManager().findFragmentByTag(LIST_FRAG);
            if (comicList == null){
                comicList = new ComicListFragment();
            }
            comicDetails (ComicDetailsFragment)getSupportFragmentManager().findFragmentByTag(DETAILS_FRAG);
            if (comicDetails == null){
                comicDetails  = new ComicDetailsFragment();
            }
        }

        updateFragmentViewState(userMode);
    }

    private void updateFragmentViewState(UserMode targetMode) {
        switch (targetMode){
            case LIST_VIEW:
                userMode = UserMode.LIST_VIEW;
                switchFragment(targetMode);
                break;
            case DETAIL_VIEW:
                userMode = UserMode.DETAIL_VIEW;
                switchFragment(targetMode);
                break;
        }
    }

    private void switchFragment(UserMode targetMode) {
        if (phoneMode == PhoneMode.PORTRAIT){
            switch (targetMode){
                case LIST_VIEW:
                    listContainer.setVisibility(View.VISIBLE);
                    detailsContainer.setVisibility(View.GONE);
                    changeDetailContainerFragment(UserMode.DETAIL_VIEW);
                    break;
                case DETAIL_VIEW:
                    listContainer.setVisibility(View.GONE);
                    detailsContainer.setVisibility(View.VISIBLE);
                    changeDetailContainerFragment(targetMode);
            }
        } else {
            if (targetMode == UserMode.LIST_VIEW) {
                changeDetailContainerFragment(UserMode.DETAIL_VIEW);
            } else {
                changeDetailContainerFragment(targetMode);
            }
        }
    }
}

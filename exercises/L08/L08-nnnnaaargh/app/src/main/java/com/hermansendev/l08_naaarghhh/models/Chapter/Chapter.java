package com.hermansendev.l08_naaarghhh.models.Chapter;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.hermansendev.l08_naaarghhh.models.Comic.Comic;

@Entity(
        tableName = "Chapter",
        foreignKeys = @ForeignKey(
                entity = Comic.class, parentColumns = "id",
                childColumns = "comic_id",
                onDelete = ForeignKey.NO_ACTION
        )
)
public class Chapter {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int chapterId;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "issue")
    private int issue;

    @ColumnInfo(name = "comic_id")
    private int comicId;

    /**
     * @param name
     * @param issue
     */
    public Chapter(String name, int issue, int comicId) {
        this.name = name;
        this.issue = issue;
        this.comicId = comicId;
    }

    public int getComicId() {
        return comicId;
    }

    public void setComicId(int comicId) {
        this.comicId = comicId;
    }

    public int getIssue() {
        return issue;
    }

    public void setIssue(int issue) {
        this.issue = issue;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }
}

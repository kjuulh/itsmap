package com.hermansendev.l08_naaarghhh.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hermansendev.l08_naaarghhh.R;
import com.hermansendev.l08_naaarghhh.models.Comic.Comic;

import java.lang.reflect.Field;

public class ComicDetailsFragment extends Fragment {
    private TextView txtTitle;
    private TextView txtDescription;

    private ComicSelectorInterface comicSelector;

    public ComicDetailsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceBundle){
        View view = inflater.inflate(R.layout.fragment_comic_details, container, false);

        txtTitle = (TextView)view.findViewById(R.id.txt_comicDetails_title);
        txtDescription = (TextView)view.findViewById(R.id.txt_comicDetails_description);

        updateComic();

        return view;
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);

        try {
            comicSelector = (ComicSelectorInterface) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement ComicSelectorInterface");
        }
    }

    public void setComic(Comic comic){
        if (txtTitle != null && txtDescription != null){
            txtTitle.setText(comic.getTitle());
            txtDescription.setText(comic.getDescription());
        }
    }

    public void updateComic(){
        if (comicSelector != null){
            setComic(comicSelector.getCurrentSelection());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}

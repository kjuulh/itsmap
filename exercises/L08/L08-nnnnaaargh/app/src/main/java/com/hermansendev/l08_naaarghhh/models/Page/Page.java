package com.hermansendev.l08_naaarghhh.models.Page;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.hermansendev.l08_naaarghhh.models.Chapter.Chapter;

@Entity(
        tableName = "Page",
        foreignKeys = @ForeignKey(
                entity = Chapter.class,
                parentColumns = "id",
                childColumns = "chapter_id",
                onDelete = ForeignKey.NO_ACTION
        )
)
public class Page {
    private static final String TAG = "NARGH_PAGE_TAG";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int Id;

    @ColumnInfo(name = "page_number")
    private int pageNumber = -1;

    @ColumnInfo(name = "content")
    private String content;

    @ColumnInfo(name = "chapter_id")
    private int chapterId;

    public Page(int pageNumber, String content, int chapterId) {
        setPageNumber(pageNumber);
        this.content = content;
        this.chapterId = chapterId;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return Id;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public void setId(int id) {
        Id = id;
    }
}

package com.hermansendev.l08_naaarghhh.models.Comic;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(
        tableName = "Comic"
)
public class Comic {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int comicId;

    @ColumnInfo(name = "title")
    @NonNull
    private String title = "";

    @ColumnInfo(name = "description")
    @NonNull
    private String description = "";

    /**
     * @param title
     * @param description
     */
    public Comic(String title, String description) {
        setTitle(title);
        setDescription(description);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getComicId() {
        return comicId;
    }

    public void setComicId(int comicId) {
        this.comicId = comicId;
    }
}

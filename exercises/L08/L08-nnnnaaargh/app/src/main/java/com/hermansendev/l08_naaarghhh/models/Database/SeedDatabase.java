package com.hermansendev.l08_naaarghhh.models.Database;

import com.hermansendev.l08_naaarghhh.models.Chapter.Chapter;
import com.hermansendev.l08_naaarghhh.models.Comic.Comic;
import com.hermansendev.l08_naaarghhh.models.Page.Page;

import java.util.ArrayList;
import java.util.List;

public class SeedDatabase {

    private AppDatabase database;

    public SeedDatabase(AppDatabase database){

        this.database = database;
    }

    public void clear(){
        database.clearAllTables();
    }

    public void addComics(){

        ArrayList<Comic> comics = new ArrayList<>();
        comics.add(new Comic("Feng Shen Ji", "A man dies..."));

        database.comicDao().insertAll(comics.toArray(new Comic[comics.size()]));
    }

    public void addChapters(){
        Comic comic = database.comicDao().findByTitle("Feng Shen Ji");

        ArrayList<Chapter> chapters = new ArrayList<>();
        chapters.add(new Chapter("Prologue", 0,  comic.getComicId()));
        chapters.add(new Chapter("Chapter 1", 1, comic.getComicId()));
        chapters.add(new Chapter("Chapter 2", 2, comic.getComicId()));
        chapters.add(new Chapter("Chapter 3", 3, comic.getComicId()));
        chapters.add(new Chapter("Chapter 4", 4, comic.getComicId()));

        database.chapterDao().insertAll(chapters.toArray(new Chapter[chapters.size()]));
    }

    public void addPages() {
        Comic comic = database.comicDao().findByTitle("Feng Shen Ji");
        Chapter chapter = database.chapterDao().findByComicAndName(comic.getComicId(), "Prologue");

        ArrayList<Page> pages = new ArrayList<>();
        pages.add(new Page(0, "Page 1", chapter.getChapterId()));
        pages.add(new Page(1, "Page 2", chapter.getChapterId()));
        pages.add(new Page(2, "Page 3", chapter.getChapterId()));
        pages.add(new Page(3, "Page 4", chapter.getChapterId()));
        pages.add(new Page(4, "Page 5", chapter.getChapterId()));
        pages.add(new Page(5, "Page 6", chapter.getChapterId()));
        pages.add(new Page(6, "Page 7", chapter.getChapterId()));

        database.pageDao().insertAll(pages.toArray(new Page[pages.size()]));
        List<Page> pagesList = database.pageDao().getAllForChapter(chapter.getChapterId());
    }
}

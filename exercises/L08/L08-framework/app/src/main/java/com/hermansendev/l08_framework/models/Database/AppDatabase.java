package com.hermansendev.l08_framework.models.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.hermansendev.l08_framework.models.Chapter.Chapter;
import com.hermansendev.l08_framework.models.Chapter.ChapterDao;
import com.hermansendev.l08_framework.models.Comic.Comic;
import com.hermansendev.l08_framework.models.Comic.ComicDao;
import com.hermansendev.l08_framework.models.Page.Page;
import com.hermansendev.l08_framework.models.Page.PageDao;


@Database(entities = {Comic.class, Chapter.class, Page.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public AppDatabase() {}

    public static AppDatabase getInstance(Context context){ // TODO: create long running context, beaware of context switching

        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, "comics_database")
                    .allowMainThreadQueries() // TODO: It's dangerous to run on mainthread, consider async
                    .build();
        }

        return instance;
    }

    public abstract ComicDao comicDao();
    public abstract ChapterDao chapterDao();
    public abstract PageDao pageDao();

}

package com.hermansendev.l08_framework.models.Page;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PageDao {
    @Query("SELECT * FROM Page")
    List<Page> GetAll();

    @Query("SELECT * FROM Page WHERE id LIKE :id LIMIT 1")
    Page findById(int id);

    @Query("SELECT * FROM Page WHERE chapter_id LIKE :chapterId AND page_number LIKE :number LIMIT 1")
    Page findByChapterAndNumber(int chapterId, int number);

    @Query("SELECT * FROM Page WHERE chapter_id LIKE :chapterId")
    List<Page> getAllForChapter(int chapterId);

    @Insert
    void insertAll(Page... pages);

    @Delete
    void delete(Page page);
}

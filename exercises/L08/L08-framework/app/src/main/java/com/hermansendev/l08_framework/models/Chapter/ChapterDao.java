package com.hermansendev.l08_framework.models.Chapter;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ChapterDao {

    @Query("SELECT * FROM Chapter")
    List<Chapter> GetAll();

    @Query("SELECT * FROM Chapter WHERE id LIKE :id LIMIT 1")
    Chapter findById(int id);

    @Query("SELECT * FROM Chapter WHERE comic_id LIKE :comicId AND  name LIKE :name LIMIT 1")
    Chapter findByComicAndName(int comicId, String name);

    @Query("SELECT * FROM Chapter WHERE comic_id LIKE :comicId")
    List<Chapter> getAllForComic(int comicId);

    @Insert
    void insertAll(Chapter... chapters);

    @Delete
    void delete(Chapter chapter);
}

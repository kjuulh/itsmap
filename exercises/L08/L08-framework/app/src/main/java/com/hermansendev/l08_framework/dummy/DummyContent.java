package com.hermansendev.l08_framework.dummy;

import android.annotation.SuppressLint;

import com.hermansendev.l08_framework.models.Comic.Comic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /*
    public static final List<Comic> ITEMS = new ArrayList<Comic>();
    public static final Map<String, Comic> ITEM_MAP = new HashMap<String, Comic>();

    private static final int COUNT = 25;
    */
    /*
    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }
    */

    /*
    @SuppressLint("DefaultLocale")
    private static void addItem(Comic item) {
        ITEMS.add(item);
        ITEM_MAP.put(String.valueOf(item.getComicId()), item);
    }
    */
    /*
    private static Comic createDummyItem(int position) {
        return new Comic(String.valueOf(position), "Item " + position, makeDetails(position));
    }
    */

    /*
    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }
    */

    /**
     * A dummy item representing a piece of content.
     */
    /*
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
    */
}

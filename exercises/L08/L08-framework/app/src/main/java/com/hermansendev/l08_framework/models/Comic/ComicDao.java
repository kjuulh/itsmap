package com.hermansendev.l08_framework.models.Comic;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ComicDao {
    @Query("SELECT * FROM Comic")
    List<Comic> GetAll();

    @Query("SELECT * FROM Comic WHERE title LIKE :title LIMIT 1")
    Comic findByTitle(String title);

    @Query("SELECT * FROM Comic WHERE id LIKE :id LIMIT 1")
    Comic findById(int id);

    @Insert
    void insertAll(Comic... comics);

    @Delete
    void delete(Comic comic);
}

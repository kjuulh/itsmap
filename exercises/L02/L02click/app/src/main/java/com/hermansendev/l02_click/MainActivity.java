package com.hermansendev.l02_click;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button IncrementBtn;
    TextView CounterText;
    Integer Counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IncrementBtn = findViewById(R.id.IncrementBtn);
        CounterText = findViewById(R.id.CounterTxt);

        IncrementBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Counter++;
                refreshUI();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(getName(), Counter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Counter = savedInstanceState.getInt(getName());

        refreshUI();
    }

    private void refreshUI(){
        CounterText.setText(String.format("%d", Counter));
    }

    private String getName() {
        return Counter.getClass().getName();
    }
}

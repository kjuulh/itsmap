package com.hermansendev.l02_selfie_app;

import android.app.Activity;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    private ImageView leftView;
    private ImageView rightView;
    private Button btnSwap;

    private Integer leftImage;
    private Integer rightImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {// first run
            leftImage = R.drawable.doge_gay;
            rightImage = R.drawable.doge_more_gay;
        }
        else {
            leftImage = savedInstanceState.getInt("LeftImage");
            rightImage = savedInstanceState.getInt("RightImage");
        }

        instantiateUI();
        refreshUI();

        btnSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swap();
            }
        });
    }

    private void swap(){
        int tempVar = leftImage;
        leftImage = rightImage;
        rightImage = tempVar;

        refreshUI();
    }

    private void instantiateUI() {
        leftView = findViewById(R.id.imageLeft);
        rightView = findViewById(R.id.imageRight);
        btnSwap = findViewById(R.id.btnSwap);
    }

    private void refreshUI() {
        leftView.setImageResource(leftImage);
        rightView.setImageResource(rightImage);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("LeftImage", leftImage);
        outState.putInt("RightImage", rightImage);
    }

}

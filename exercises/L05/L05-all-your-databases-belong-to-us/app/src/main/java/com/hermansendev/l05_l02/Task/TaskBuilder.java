package com.hermansendev.l05_l02.Task;

public class TaskBuilder {
    public static Task construct(String name, String place){
        Task task = new Task();
        task.TaskName = name;
        task.Place = place;
        return task;
    }
}

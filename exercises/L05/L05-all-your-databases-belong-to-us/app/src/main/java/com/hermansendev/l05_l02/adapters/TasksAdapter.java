package com.hermansendev.l05_l02.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hermansendev.l05_l02.Task.Task;

import java.util.List;

public class TasksAdapter extends BaseAdapter {
    Context context;
    List<Task> tasksList;
    Task task;

    public TasksAdapter(Context context, List<Task> tasksList){
        this.context = context;
        this.tasksList = tasksList;
    }

    @Override
    public int getCount() {
        return tasksList.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < tasksList.size()){
            return tasksList.get(position);
        } else {
            return null;
        }

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}

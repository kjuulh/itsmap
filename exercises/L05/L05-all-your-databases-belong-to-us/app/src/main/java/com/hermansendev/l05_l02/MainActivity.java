package com.hermansendev.l05_l02;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hermansendev.l05_l02.Task.AppDatabase;
import com.hermansendev.l05_l02.Task.Task;
import com.hermansendev.l05_l02.Task.TaskBuilder;
import com.hermansendev.l05_l02.Task.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TaskRepository taskRepository;


    Button AddBtn;
    EditText taskText;
    EditText placeText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.taskRepository = TaskRepository.getInstance(getApplicationContext());

        taskText = findViewById(R.id.text_main_task);
        placeText = findViewById(R.id.text_main_place);
        AddBtn = findViewById(R.id.btn_main_add);

        AddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnClick();
            }
        });
    }

    private void handleOnClick() {
        if(!taskRepository.insertAll(TaskBuilder.construct(
                taskText.getText().toString(),
                placeText.getText().toString()
        ))){
            Toast.makeText(this, "Duplicate found", Toast.LENGTH_SHORT);
        }

        List<Task> tasks = taskRepository.getAll();
    }


}

package com.hermansendev.l05_l02.Task;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Task {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "task_name")
    public String TaskName;

    @ColumnInfo(name = "place")
    public String Place;
}

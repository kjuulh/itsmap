package com.hermansendev.l05_l02.Task;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Task.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase
{
    public abstract TaskDAO taskDAO();
}

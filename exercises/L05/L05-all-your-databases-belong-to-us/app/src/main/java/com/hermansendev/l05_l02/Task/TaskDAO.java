package com.hermansendev.l05_l02.Task;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface TaskDAO {
    @Query("SELECT * FROM task")
    List<Task> getAll();

    @Query("SELECT * FROM task where task_name LIKE :taskName LIMIT 1")
    Task findByName(String taskName);

    @Insert
    void insertAll(Task... tasks);

    @Delete
    void delete(Task task);
}

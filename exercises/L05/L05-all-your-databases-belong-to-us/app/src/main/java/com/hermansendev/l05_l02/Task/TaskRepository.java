package com.hermansendev.l05_l02.Task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import androidx.room.Room;

public class TaskRepository {

    private static final String TAG = "TaskRepository";
    private AppDatabase db;

    private static TaskRepository instance;

    private TaskRepository() { }

    public static TaskRepository getInstance(Context context) {

        if (instance == null) {
            instance = new TaskRepository();
            instance.db = Room.databaseBuilder(context, AppDatabase.class, "task_database")
                    .allowMainThreadQueries()
                    .build();
        }

        return instance;
    }

    public List<Task> getAll(){

        return db.taskDAO().getAll();
    }

    public Task findByName(String taskName){
        return db.taskDAO().findByName(taskName);
    }

    public boolean insertAll(Task... tasks){
        try 
        {
            db.taskDAO().insertAll(tasks);
            return true;
        }
        catch (Exception e) {
            Log.d(TAG, "insertAll: " + e.getMessage());
            return false;
        }
    }

    public void delete(Task task) {
        db.taskDAO().delete(task);
    }
}


package com.hermansendev.l07_network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private static final String WEATHER_API_KEY = "84101b87a3d1a788a66568659cd54b06";
    public static final long CITY_ID_AARHUS = 2624652;
    public static final String WEATHER_API_CALL_HTTPS = "https://api.openweathermap.org/data/2.5/weather?id=" + CITY_ID_AARHUS + "&APPID=" + WEATHER_API_KEY;

    private String TAG = "MainActivity";

    private TextView txtResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button BtnGetConInfo = findViewById(R.id.button_main_getConInfo);
        BtnGetConInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getConnectionInfoAndDisplay();
            }
        });

        Button btnGetWeatherAarhus = findViewById(R.id.button_main_getWeatherAarhus);
        btnGetWeatherAarhus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherAarhusAndDisplay();
            }
        });

        txtResponse = findViewById(R.id.txt_main_weather_result);
    }

    private void getWeatherAarhusAndDisplay() {
        DownloadTask dT = new DownloadTask();

        dT.execute(WEATHER_API_CALL_HTTPS);
    }

    private class DownloadTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            Log.d(TAG, "doInBackground: Starting");
            return callUrls(urls[0]);
        }

        @Override
        protected void onPostExecute(String result){
            if (result != null){
                txtResponse.setText(result);
            } else {
                Log.d(TAG, "onPostExecute: Didn't receive anything");
            }
        }
    }

    private String callUrls(String rawUrl) {
        InputStream is = null;

        try {
            URL url = new URL(rawUrl);

            HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();

            conn.setReadTimeout(100000);
            conn.setConnectTimeout(150000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            conn.connect();
            int response = conn.getResponseCode();
            if (response != HttpsURLConnection.HTTP_OK) {
                Log.d(TAG, "callUrls: " + response);
            }

            is = conn.getInputStream();

            return convertStreamToStringBuffered(is);

        } catch (ProtocolException pe) {
            Log.d(TAG, "ProtocolException");
        } catch (UnsupportedEncodingException uee) {
            Log.d(TAG, "UnsuportedEncodingException");
        } catch (IOException ioe) {
            Log.d(TAG, "IOException");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    Log.d(TAG, "Could not close stream, IOException");
                }
            }
        }
        return null;
    }

    private String convertStreamToStringBuffered(InputStream is) {
        StringBuilder s = new StringBuilder();
        String line;

        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((line = rd.readLine()) != null) { s.append(line); }
        } catch (IOException ex) {
            Log.e(TAG, "ERROR reading HTTP response", ex);
        }

        return s.toString();
    }

    private void getConnectionInfoAndDisplay() {
        ConnectivityManager conMan = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            Toast.makeText(this, netInfo.toString(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "No Connections", Toast.LENGTH_SHORT).show();
        }
    }
}

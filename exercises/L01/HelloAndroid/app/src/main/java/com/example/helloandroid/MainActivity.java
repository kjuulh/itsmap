package com.example.helloandroid;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void changeMessage(View view){
        TextView textView = (TextView)findViewById(R.id.textView_Main);
        textView.setText(R.string.change_message);
    }

    public void exit(View view){
        finish();
        moveTaskToBack(true);
    }
}

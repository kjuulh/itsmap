package com.hermansen.thegameofwar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.EventListener;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public int state;
    public WarController warController;

    public Button actionBtn;
    public TextView mainTxt;

    public TextView p1CardNameText;
    public TextView p1CardValueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Deck deck = new Deck();
        List<Card> cards = deck.shuffleCards();
        warController = new WarController(cards);

        actionBtn = findViewById(R.id.btnAction);
        mainTxt = findViewById(R.id.main_text);

        p1CardNameText = findViewById(R.id.p1CardNameTxt);
        p1CardValueText = findViewById(R.id.p1CardValueTxt);

        actionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playGame();
            }
        });
    }

    public void playGame(){
        warController.play();

        switch (warController.state){
            case READY:
                actionBtn.setText("Draw!");
                mainTxt.setText("Fight!");
                break;
            case DRAW:
                actionBtn.setText("Fight!");
                mainTxt.setText("Fight!");

                p1CardNameText.setText(warController.PlayerOne.Field.Name);
                p1CardValueText.setText(warController.PlayerOne.Field.Value);
                break;
        }
    }
}

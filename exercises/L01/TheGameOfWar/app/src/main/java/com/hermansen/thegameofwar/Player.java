package com.hermansen.thegameofwar;

import java.util.ArrayList;
import java.util.List;

class Player {
    public List<Card> Deck;
    public Card Field;
    public List<Card> WarField = new ArrayList<Card>();

    public Player(List<Card> deck){
        Deck = deck;
    }

    public void draw(){
        Field = Deck.remove(0);
    }

    public void wins(List<Card> cards){
        Field = null;
        WarField = new ArrayList<Card>();
        Deck.addAll(cards);
    }

    public void loses(){
        Field = null;
        WarField = new ArrayList<Card>();
    }

    public ArrayList<Card> drawThree(){
        ArrayList<Card> tmpList = new ArrayList<Card>();

        tmpList.add(Deck.remove(0));
        tmpList.add(Deck.remove(0));
        tmpList.add(Deck.remove(0));

        WarField.addAll(tmpList);
        return tmpList;

    }
}

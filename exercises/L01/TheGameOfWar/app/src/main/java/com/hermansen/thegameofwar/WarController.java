package com.hermansen.thegameofwar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WarController {

    public enum States {
        READY,
        DRAW,
        WAR,
        WAR_PICKED_CARD,
        WAR_TIE
    }

    public Player PlayerOne;
    public Player PlayerTwo;

    public States state;

    public List<Card> batch = new ArrayList<>();


    public WarController(List<Card> deck) {
        if (deck.size() != 52) {
            throw new IllegalArgumentException("The Deck doesn't have 52 cards");
        }
        ArrayList<Card> playerOneList = new ArrayList<Card>();
        ArrayList<Card> playerTwoList = new ArrayList<Card>();

        for (int i = 0; i < deck.size() / 2; i++) {
            playerOneList.add(deck.get(i));
            playerTwoList.add(deck.get(i + deck.size() / 2));
        }

        PlayerOne = new Player(playerOneList);
        PlayerTwo = new Player(playerTwoList);

        setState(States.READY);
    }

    public void play() {
        switch (state) {
            case READY:
                draw();
                break;
            case DRAW:
                playGame();
                break;
            case WAR:
                pickCard(new Random().nextInt(3));
                playGame();
                break;
            case WAR_TIE:
                break;
            case WAR_PICKED_CARD:
                playGame();
                break;
        }
    }

    public void setState(States state) {
        this.state = state;
    }

    public void draw() {
        setState(States.DRAW);
        PlayerOne.draw();
        PlayerTwo.draw();
    }

    public void playGame() {
        System.out.println("Player One: " + PlayerOne.Field.Name + " vs Player Two: " + PlayerTwo.Field.Name);
        if (PlayerOne.Field.Value > PlayerTwo.Field.Value) {
            System.out.println("Player One Wins Round");
            if (state == States.WAR_PICKED_CARD || state == States.WAR_TIE) {
                PlayerOne.wins(batch);
                PlayerTwo.loses();
            }
            ArrayList<Card> game = new ArrayList<Card>();
            game.add(PlayerOne.Field);
            game.add(PlayerTwo.Field);

            PlayerOne.wins(game);
            PlayerTwo.loses();
            setState(States.READY);
        } else if (PlayerOne.Field.Value < PlayerTwo.Field.Value) {
            System.out.println("Player two Wins Round");
            if (state == States.WAR_PICKED_CARD || state == States.WAR_TIE) {
                PlayerTwo.wins(batch);
                PlayerOne.loses();
            }
            ArrayList<Card> game = new ArrayList<Card>();
            game.add(PlayerOne.Field);
            game.add(PlayerTwo.Field);

            PlayerTwo.wins(game);
            PlayerOne.loses();
            setState(States.READY);
        } else {
            if (state == States.WAR_PICKED_CARD || state == States.WAR_TIE) {
                setState(States.WAR_TIE);
                System.out.println("It's a tie. Enter sudden death");
                batch.add(PlayerOne.Field);
                batch.add(PlayerTwo.Field);
                PlayerOne.Field = null;
                PlayerTwo.Field = null;

                draw();
                playGame();
            } else {
                System.out.println("It's a tie. Enter War");
                war();
            }
        }
    }

    public void war() {
        this.batch.add(PlayerOne.Field);
        this.batch.add(PlayerTwo.Field);
        PlayerOne.Field = null;
        PlayerTwo.Field = null;

        PlayerOne.drawThree();
        PlayerTwo.drawThree();

        setState(States.WAR);

    }

    public void pickCard(int number) {
        if (number >= 0 && number <= 3) {
            PlayerOne.Field = PlayerOne.WarField.get(number);
            int playerTwoNumber = new Random().nextInt(3);
            PlayerTwo.Field = PlayerTwo.WarField.get(playerTwoNumber);

            for (int i = 0; i < 3; i++) {
                if (i != number)
                    batch.add(PlayerOne.WarField.get(i));
                if (i != playerTwoNumber)
                    batch.add(PlayerTwo.WarField.get(i));
            }
            PlayerOne.WarField = new ArrayList<Card>();
            PlayerTwo.WarField = new ArrayList<Card>();
            setState(States.WAR_PICKED_CARD);
        }

    }
}

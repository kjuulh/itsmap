package com.hermansen.thegameofwar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {

    public List<Card> shuffleCards() {
        return shuffleCards(true);
    }

    public List<Card> shuffleCards(boolean randomShuffe) {
        List<Card> cards = new ArrayList<Card>();
        List<Card> tmpList = prepareCards();

        cards.clear();

        if (randomShuffe) {
            Random r = new Random();

            while (!tmpList.isEmpty()) {
                int index = r.nextInt(tmpList.size());
                cards.add(tmpList.get(index));
                tmpList.remove(index);
            }
        }
        else
        {
            cards = tmpList;
        }

        return cards;
    }

    private ArrayList<Card> prepareCards() {
        ArrayList<Card> tmpList = new ArrayList<Card>();
        // Hearts
        tmpList.add(new Card("Two of Hearts", 2));
        tmpList.add(new Card("Three of Hearts", 3));
        tmpList.add(new Card("Four of Hearts", 4));
        tmpList.add(new Card("Five of Hearts", 5));
        tmpList.add(new Card("Six of Hearts", 6));
        tmpList.add(new Card("Seven of Hearts", 7));
        tmpList.add(new Card("Eight of Hearts", 8));
        tmpList.add(new Card("Nine of Hearts", 9));
        tmpList.add(new Card("Ten of Hearts", 10));
        tmpList.add(new Card("Jack of Hearts", 11));
        tmpList.add(new Card("Queen of Hearts", 12));
        tmpList.add(new Card("King of Hearts", 13));
        tmpList.add(new Card("Ace of Hearts", 14));

        // Diamonds
        tmpList.add(new Card("Two of Diamonds", 2));
        tmpList.add(new Card("Three of Diamonds", 3));
        tmpList.add(new Card("Four of Diamonds", 4));
        tmpList.add(new Card("Five of Diamonds", 5));
        tmpList.add(new Card("Six of Diamonds", 6));
        tmpList.add(new Card("Seven of Diamonds", 7));
        tmpList.add(new Card("Eight of Diamonds", 8));
        tmpList.add(new Card("Nine of Diamonds", 9));
        tmpList.add(new Card("Ten of Diamonds", 10));
        tmpList.add(new Card("Jack of Diamonds", 11));
        tmpList.add(new Card("Queen of Diamonds", 12));
        tmpList.add(new Card("King of Diamonds", 13));
        tmpList.add(new Card("Ace of Diamonds", 14));

        // Clubs
        tmpList.add(new Card("Two of Clubs", 2));
        tmpList.add(new Card("Three of Clubs", 3));
        tmpList.add(new Card("Four of Clubs", 4));
        tmpList.add(new Card("Five of Clubs", 5));
        tmpList.add(new Card("Six of Clubs", 6));
        tmpList.add(new Card("Seven of Clubs", 7));
        tmpList.add(new Card("Eight of Clubs", 8));
        tmpList.add(new Card("Nine of Clubs", 9));
        tmpList.add(new Card("Ten of Clubs", 10));
        tmpList.add(new Card("Jack of Clubs", 11));
        tmpList.add(new Card("Queen of Clubs", 12));
        tmpList.add(new Card("King of Clubs", 13));
        tmpList.add(new Card("Ace of Clubs", 14));

        // Spades
        tmpList.add(new Card("Two of Spades", 2));
        tmpList.add(new Card("Three of Spades", 3));
        tmpList.add(new Card("Four of Spades", 4));
        tmpList.add(new Card("Five of Spades", 5));
        tmpList.add(new Card("Six of Spades", 6));
        tmpList.add(new Card("Seven of Spades", 7));
        tmpList.add(new Card("Eight of Spades", 8));
        tmpList.add(new Card("Nine of Spades", 9));
        tmpList.add(new Card("Ten of Spades", 10));
        tmpList.add(new Card("Jack of Spades", 11));
        tmpList.add(new Card("Queen of Spades", 12));
        tmpList.add(new Card("King of Spades", 13));
        tmpList.add(new Card("Ace of Spades", 14));

        return tmpList;
    }
}

package com.hermansendev.l06_at_your_service.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class backgroundService extends Service {

    public static final String BROADCAST_BACKGROUND_SERVICE_RESULT = "com.hermansendev.l06_at_your_service.BROADCAST_BACKGROUND_SERVICE_RESULT";
    public static final String EXTRA_TASK_RESULT = "task_result";
    public static final String EXTRA_TASK_TIME_MS = "task_time";
    private static final String LOG = "BG_SERVICE";
    private static final int NOTIFY_ID = 42;

    private boolean started = false;
    private long wait;

    private static final long DEFAULT_WAIT = 30*100;

    private boolean runAsForegroundService = true;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG, "Backgrond service OnCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!started && intent != null) {
            wait = intent.getLongExtra(EXTRA_TASK_TIME_MS, DEFAULT_WAIT);
            Log.d(LOG, "onStartCommand: with wait: " + wait + "ms");
            started = true;
            
            doBackgroundThing(wait);
        } else {
            Log.d(LOG, "onStartCommand: already started");
        }
        return START_STICKY;
    }

    private void doBackgroundThing(final long waitTimeInMillis) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Object, Object, String> task = new AsyncTask<Object, Object, String>() {
            @Override
            protected  void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Object[] params){
                String s  = "Background job";
                try {
                    Log.d(LOG, "doInBackground: Started");
                    Thread.sleep(waitTimeInMillis);
                    Log.d(LOG, "doInBackground: Completed");
                } catch (Exception e) {
                    s+=" did not finish due to error";
                    return s;
                }

                s += " completed after " + waitTimeInMillis + "ms";
                return s;
            }

            @Override
            protected void onPostExecute(String stringResult) {
                super.onPostExecute(stringResult);
                broadcastTaskResult(stringResult);

                if (started) {
                    doBackgroundThing(waitTimeInMillis);
                }
            }
        };
        task.execute();
    }

    private void broadcastTaskResult(String result) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(BROADCAST_BACKGROUND_SERVICE_RESULT);
        broadcastIntent.putExtra(EXTRA_TASK_RESULT, result);
        Log.d(LOG, "broadcastTaskResult: " + result);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        started = false;
        Log.d(LOG, "onDestroy: service destroyed");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

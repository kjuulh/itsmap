package com.hermansendev.l06_at_your_service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hermansendev.l06_at_your_service.services.BoundCountingService;
import com.hermansendev.l06_at_your_service.services.backgroundService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN";

    private Button btnStartBgService, btnStopBgService, btnGetCount;

    private long task_time = 4 * 1000;

    //for bound counting service
    private BoundCountingService countingService;
    private ServiceConnection countingServiceConnection;
    private boolean bound = false;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStartBgService = (Button) findViewById(R.id.button_main_start);
        btnStartBgService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBackgroundService(task_time);
                bindToCountingService();
            }
        });

        btnStopBgService = (Button) findViewById(R.id.button_main_stop);
        btnStopBgService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopBackgroundService(task_time);
                unBindFromCountingService();
            }
        });

        btnGetCount = (Button) findViewById(R.id.button_main_getCount);
        btnGetCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bound && countingService != null) {
                    count = countingService.getCount();

                    Toast.makeText(MainActivity.this, "Count is " + count, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Not bound yet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        setupConnectionToCountingService();
    }

    private void setupConnectionToCountingService(){
        countingServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                // This is called when the connection with the service has been
                // established, giving us the service object we can use to
                // interact with the service.  Because we have bound to a explicit
                // service that we know is running in our own process, we can
                // cast its IBinder to a concrete class and directly access it.
                //ref: http://developer.android.com/reference/android/app/Service.html
                countingService = ((BoundCountingService.CountingServiceBinder)service).getService();
                Log.d(TAG, "Counting service connected");

            }

            public void onServiceDisconnected(ComponentName className) {
                // This is called when the connection with the service has been
                // unexpectedly disconnected -- that is, its process crashed.
                // Because it is running in our same process, we should never
                // see this happen.
                //ref: http://developer.android.com/reference/android/app/Service.html
                countingService = null;
                Log.d(TAG, "Counting service disconnected");
            }
        };
    }


    private void bindToCountingService() {
        bindService(
                new Intent(MainActivity.this,
                BoundCountingService.class), countingServiceConnection, Context.BIND_AUTO_CREATE
        );
        bound = true;

    }

    private void unBindFromCountingService() {
        if (bound) {
            // Detach our existing connection.
            unbindService(countingServiceConnection);
            bound = false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Registering receivers");

        IntentFilter filter = new IntentFilter();
        filter.addAction(backgroundService.BROADCAST_BACKGROUND_SERVICE_RESULT);

        LocalBroadcastManager.getInstance(this).registerReceiver(onBackgroundServiceResult, filter);
    }

    private void startBackgroundService(long task_time) {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, backgroundService.class);
        backgroundServiceIntent.putExtra(backgroundService.EXTRA_TASK_TIME_MS, task_time);
        startService(backgroundServiceIntent);
    }

    private void stopBackgroundService(long task_time) {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, backgroundService.class);
        stopService(backgroundServiceIntent);
    }

    private BroadcastReceiver onBackgroundServiceResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Broadcast received from bg service");
            String result = intent.getStringExtra(backgroundService.EXTRA_TASK_RESULT);
            if (result == null) {
                result = "Error background service";
            }
            handleBackgroundResult(result);
        }
    };

    private void handleBackgroundResult(String result) {
        Toast.makeText(MainActivity.this, "Got result from background service:\n" + result, Toast.LENGTH_SHORT).show();
    }
}

package com.hermansendev.l06_at_your_service.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class BoundCountingService extends Service {
    private int count;
    private boolean running = false;

    public class CountingServiceBinder extends Binder {
        public BoundCountingService getService() {
            return BoundCountingService.this;
        }
    }

    private final IBinder binder = new CountingServiceBinder();

    @Override
    public void onCreate() {
        super.onCreate();

        count = 0;
        running = true;

        Runnable r = new Runnable() {
            @Override
            public void run() {
                while(running) {
                    count++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public int getCount() {
        return count;
    }
}

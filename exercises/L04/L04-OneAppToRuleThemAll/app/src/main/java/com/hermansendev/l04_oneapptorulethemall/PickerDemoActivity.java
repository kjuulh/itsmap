package com.hermansendev.l04_oneapptorulethemall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.SeekBar;

/**
 * Let the user choose a number between 0 and 1000 using a Slider og NumberPicker control
 * Add OK and Cancel buttons
 * If the user presses OK, return the int value chosen and show it in the main activity
 * USE PICKER INSTEAD OF EDITTEXT!!!!!
 */
public class PickerDemoActivity extends AppCompatActivity {

    public static final String IS_NEITHER = "IS_NEITHER";
    public static final int MAX_VALUE = 1000;
    public static final int MIN_VALUE = 0;
    Button AcceptButton;
    Button CancelButton;

    SeekBar numbersSlider;
    NumberPicker numberPicker;

    private int number;
    private String state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker_demo);

        getWidgets();
        setOnClickListener();

        updateNumber((int)Math.round(MAX_VALUE-MIN_VALUE*0.5), IS_NEITHER);
    }

    private void getWidgets() {
        AcceptButton = findViewById(R.id.btn_picker_accept);
        CancelButton = findViewById(R.id.btn_picker_cancel);
        numbersSlider = findViewById(R.id.sldr_picker);
        numberPicker = findViewById(R.id.editn_picker);

        numberPicker.setMinValue(MIN_VALUE);
        numberPicker.setMaxValue(MAX_VALUE);
    }

    private void setOnClickListener() {
        AcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAccept();
            }
        });
        CancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancel();
            }
        });
        numbersSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateNumber(progress, IS_NEITHER);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                updateNumber(newVal, IS_NEITHER);
            }
        });
    }

    private void updateNumber(int number, String origin) {
        this.number = number;
        switch (origin) {
            default:
                numberPicker.setValue(this.number);
                numbersSlider.setProgress(this.number);
                break;
        }
    }

    private void handleAccept() {
        Intent i = new Intent();
        i.putExtra(MainActivity.VALUES_PICKER_DEMO, Integer.toString(this.number));
        setResult(RESULT_OK, i);
        finish();
    }

    private void handleCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

package com.hermansendev.l04_oneapptorulethemall;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DemoAdapter extends BaseAdapter {

    Context context;
    ArrayList<Demo> demos;
    Demo demo;

    public DemoAdapter(Context context, ArrayList<Demo> demoList){
        this.context = context;
        this.demos = demoList;
    }

    @Override
    public int getCount() {
        return demos.size();
    }

    @Override
    public Object getItem(int position) {
        return demos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_demo_item, null);
        }

        demo = demos.get(position);
        if (demo != null){
            TextView textTitle = (TextView) convertView.findViewById(R.id.list_item_demos_title);
            textTitle.setText(demo.getTitle());

            TextView textDescription = (TextView) convertView.findViewById(R.id.list_item_demos_text);
            textDescription.setText(demo.getDescription());
        }
        return convertView;
    }
}

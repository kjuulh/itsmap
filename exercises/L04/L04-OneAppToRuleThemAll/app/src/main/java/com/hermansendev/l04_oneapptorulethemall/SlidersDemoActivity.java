package com.hermansendev.l04_oneapptorulethemall;

import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Create three Sliders and label them Red, Green and Blue (with TextViews)
 * Configure the sliders to hold values from 0-255
 * Handle the proper events in your Activity when the user changes the values of the sliders
 * On every update, use the slider values to change the background color of the root layout to the corresponding RGB color (hint: Color.rgb(int, int, int) )
 * Add OK and Cancel buttons
 * If user presses OK, return the RGB values as a String and show it in the main activity
 */
public class SlidersDemoActivity extends AppCompatActivity {

    public static final String JSON_RED = "red";
    public static final String JSON_GREEN = "green";
    public static final String JSON_BLUE = "blue";

    Button AcceptBtn;
    Button CancelBtn;

    SeekBar RedColorSlider;
    SeekBar GreenColorSlider;
    SeekBar BlueColorSlider;

    ConstraintLayout Root;

    private int red = 0;
    private int green = 0;
    private int blue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sliders_demo);

        getWidgets();
        setOnListeners();
    }

    private void getWidgets() {
        AcceptBtn = findViewById(R.id.btn_slider_accept);
        CancelBtn = findViewById(R.id.btn_slider_cancel);
        RedColorSlider = findViewById(R.id.sldr_slider_red);
        GreenColorSlider = findViewById(R.id.sldr_slider_green);
        BlueColorSlider = findViewById(R.id.sldr_slider_blue);
        Root = findViewById(R.id.bg_slider);
    }

    private void setOnListeners() {
        AcceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAccept();
            }
        });
        CancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancel();
            }
        });
        RedColorSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateRed(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        GreenColorSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateGreen(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        BlueColorSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateBlue(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void handleAccept() {
        String object = constructJson();
        Intent i = new Intent();

        i.putExtra(MainActivity.VALUES_SLIDERS_DEMO, object);
        setResult(RESULT_OK, i);
        finish();
    }

    private void handleCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void updateRed(int progress) {
        this.red = progress;
        refreshColor();
    }

    private void updateGreen(int progress) {
        this.green = progress;
        refreshColor();
    }

    private void updateBlue(int progress) {
        this.blue = progress;
        refreshColor();
    }

    private void refreshColor() {
        Root.setBackgroundColor(Color.rgb(this.red, this.green, this.blue));
    }

    private String constructJson() {
        JSONObject jColors = new JSONObject();

        try {
            jColors.put(JSON_RED, this.red);
            jColors.put(JSON_GREEN, this.green);
            jColors.put(JSON_BLUE, this.blue);

            return jColors.toString(2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

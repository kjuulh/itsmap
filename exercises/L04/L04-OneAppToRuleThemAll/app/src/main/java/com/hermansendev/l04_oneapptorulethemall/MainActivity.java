package com.hermansendev.l04_oneapptorulethemall;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Demo> demoList = new ArrayList<Demo>();

    Button PickerBtn;
    Button EditTextDemoBtn;
    Button SlidersDemoBtn;

    DemoAdapter demoAdapter;
    ListView demoListView;

    public static final int REQUEST_PICKER_DEMO = 100;
    public static final int REQUEST_EDITTEXT_DEMO = 200;
    public static final int REQUEST_SLIDERS_DEMO = 300;

    public static final String VALUES_PICKER_DEMO = "PICKER";
    public static final String VALUES_EDITTEXT_DEMO = "EDITTEXT";
    public static final String VALUES_SLIDERS_DEMO = "SLIDERS";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeDemoList();
        GetWidgets();
        SetOnClickListeners();
    }

    private void InitializeDemoList() {

        demoList.add(new Demo("Picker", "This is a demo for picking stuff", "com.hermansendev.l04_oneapptorulethemall.PICKER_DEMO", REQUEST_PICKER_DEMO));
        demoList.add(new Demo("Edit Text", "This is a demo for editting stuff", "com.hermansendev.l04_oneapptorulethemall.EDIT_TEXT_DEMO", REQUEST_EDITTEXT_DEMO));
        demoList.add(new Demo("Slider", "This is a demo for sliding stuff", "com.hermansendev.l04_oneapptorulethemall.SLIDER_DEMO", REQUEST_SLIDERS_DEMO));

        for (int i = 0; i < 1000; i++){
            demoList.add(new Demo("Title", "Something"));
        }
    }

    private void GetWidgets() {
        PickerBtn = findViewById(R.id.btn_main_picker);
        EditTextDemoBtn = findViewById(R.id.btn_main_edittext);
        SlidersDemoBtn = findViewById(R.id.btn_main_sliders);

        setListView();
    }

    private void setListView() {
        demoAdapter = new DemoAdapter(this, demoList);
        demoListView = (ListView)findViewById(R.id.list_main_demos);
        demoListView.setAdapter(demoAdapter);
    }

    private void SetOnClickListeners() {
        PickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivatePickerDemo();
            }
        });
        EditTextDemoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivateEditTextDemo();
            }
        });
        SlidersDemoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivateSlidersDemo();
            }
        });
        this.demoListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent, View view, int position, long id){
                Intent startDemoIntent = new Intent();
                startDemoIntent.putExtra("position", position);
                String action = demoList.get(position).getIntentAction();
                int demoResultCode = demoList.get(position).getResultCode();
                if(action != null && !action.equals("")) {
                    startDemoIntent.setAction(action);
                    startActivityForResult(startDemoIntent, demoResultCode);
                }
            }
        });
    }

    private void ActivatePickerDemo() {
        Intent i = new Intent(this, PickerDemoActivity.class);
        startActivityForResult(i, REQUEST_PICKER_DEMO);
    }

    private void ActivateEditTextDemo() {
        Intent i = new Intent(this, EditTextDemoActivity.class);
        startActivityForResult(i, REQUEST_EDITTEXT_DEMO);
    }

    private void ActivateSlidersDemo() {
        Intent i = new Intent(this, SlidersDemoActivity.class);
        startActivityForResult(i, REQUEST_SLIDERS_DEMO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_PICKER_DEMO:
                switch(resultCode){
                    case RESULT_OK:
                        handlePickerReturnSuccess(data);
                        break;
                    case RESULT_CANCELED:
                        handlePickerReturnCancelled(data);
                        break;
                }
                break;
            case REQUEST_EDITTEXT_DEMO:
                switch(resultCode){
                    case RESULT_OK:
                        handleEditTextReturnSuccess(data);
                        break;
                    case RESULT_CANCELED:
                        handleEditTextReturnCancelled(data);
                        break;
                }
                break;
            case REQUEST_SLIDERS_DEMO:
                switch(resultCode){
                    case RESULT_OK:
                        handleSlidersReturnSuccess(data);
                        break;
                    case RESULT_CANCELED:
                        handleSlidersReturnCancelled(data);
                        break;
                }
                break;
            default:
                Toast.makeText(this, "Wrong Request Code was returned", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void handlePickerReturnSuccess(Intent data) {
        String returnedData = data.getExtras().getString(VALUES_PICKER_DEMO);
        Toast.makeText(this, returnedData, Toast.LENGTH_SHORT).show();
    }

    private void handlePickerReturnCancelled(Intent data) {
        Toast.makeText(this, "PickerActivity was cancelled", Toast.LENGTH_SHORT).show();
    }

    private void handleEditTextReturnSuccess(Intent data) {

        ArrayList<String> returnedData = new ArrayList<>();

        String returnedUsername = data.getExtras().getString(VALUES_EDITTEXT_DEMO + EditTextDemoActivity.VALUES_EDITTEXT_DEMO_USERNAME);
        String returnedEmail = data.getExtras().getString(VALUES_EDITTEXT_DEMO + EditTextDemoActivity.VALUES_EDITTEXT_DEMO_EMAIL);
        String returnedAge = data.getExtras().getString(VALUES_EDITTEXT_DEMO + EditTextDemoActivity.VALUES_EDITTEXT_DEMO_AGE);
        String returnedPassword = data.getExtras().getString(VALUES_EDITTEXT_DEMO + EditTextDemoActivity.VALUES_EDITTEXT_DEMO_PASSWORD);

        returnedData.add("Username: "   + returnedUsername);
        returnedData.add("Email: "      + returnedEmail);
        returnedData.add("Age: "        + returnedAge);
        returnedData.add("Password: "   + returnedPassword);

        for (String d : returnedData){
            Toast.makeText(this, d, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleEditTextReturnCancelled(Intent data) {
        Toast.makeText(this, "EditTextActivity was cancelled", Toast.LENGTH_SHORT).show();
    }

    private void handleSlidersReturnSuccess(Intent data) {
        String returnedData = data.getExtras().getString(VALUES_SLIDERS_DEMO);
        Toast.makeText(this, returnedData, Toast.LENGTH_SHORT).show();
    }

    private void handleSlidersReturnCancelled(Intent data) {
        Toast.makeText(this, "SliderActivity was cancelled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        boolean overrideDefaultHandling = false;
        switch (id){
            case R.id.item_main_menu_search:
                overrideDefaultHandling = true;
                showSearchDialog();
                break;
        }
        if (overrideDefaultHandling){
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showSearchDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final EditText editTextSearch = new EditText(this);
        builder.setView(editTextSearch);

        builder.setMessage(R.string.search_dialog_message)
                .setTitle(R.string.search_dialog_title);

        builder.setPositiveButton(R.string.btn_accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleDialogAccept(editTextSearch.getText().toString());
            }
        });

        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleDialogCancel();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void handleDialogAccept(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    private void handleDialogCancel() {

    }
}

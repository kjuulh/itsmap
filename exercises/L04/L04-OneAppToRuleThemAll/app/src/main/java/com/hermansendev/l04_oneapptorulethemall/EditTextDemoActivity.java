package com.hermansendev.l04_oneapptorulethemall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Create a number of different EditText input fields and set them to specific data types: plain text, email, number and password.
 * Add an OK and Cancel buttons
 * If the user presses OK, return the values of all EditTexts and show it in the main activity
 */
public class EditTextDemoActivity extends AppCompatActivity {

    Button AcceptBtn;
    Button CancelBtn;

    EditText UserField;
    EditText EmailField;
    EditText AgeField;
    EditText PasswordField;

    public static final String VALUES_EDITTEXT_DEMO_USERNAME = "USERNAME";
    public static final String VALUES_EDITTEXT_DEMO_EMAIL = "EMAIL";
    public static final String VALUES_EDITTEXT_DEMO_AGE = "AGE";
    public static final String VALUES_EDITTEXT_DEMO_PASSWORD = "PASSWORD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_demo);

        getWidgets();

        setOnListeners();
    }

    private void getWidgets() {
        AcceptBtn = findViewById(R.id.btn_edit_accept);
        CancelBtn = findViewById(R.id.btn_edit_cancel);
        UserField = findViewById(R.id.editT_edit_username);
        EmailField = findViewById(R.id.editE_edit_email);
        AgeField = findViewById(R.id.editN_edit_age);
        PasswordField = findViewById(R.id.editP_edit_password);
    }

    private void setOnListeners() {
        AcceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAccept();
            }
        });
        CancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancel();
            }
        });
    }

    private void handleAccept() {
        Intent i = new Intent();

        i.putExtra(MainActivity.VALUES_EDITTEXT_DEMO + VALUES_EDITTEXT_DEMO_USERNAME, UserField.getText().toString());
        i.putExtra(MainActivity.VALUES_EDITTEXT_DEMO + VALUES_EDITTEXT_DEMO_EMAIL, EmailField.getText().toString());
        i.putExtra(MainActivity.VALUES_EDITTEXT_DEMO + VALUES_EDITTEXT_DEMO_AGE, AgeField.getText().toString());
        i.putExtra(MainActivity.VALUES_EDITTEXT_DEMO + VALUES_EDITTEXT_DEMO_PASSWORD, PasswordField.getText().toString());

        setResult(RESULT_OK, i);
        finish();
    }

    private void handleCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

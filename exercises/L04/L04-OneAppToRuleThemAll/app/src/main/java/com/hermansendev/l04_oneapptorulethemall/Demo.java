package com.hermansendev.l04_oneapptorulethemall;

public class Demo {
    private String title;
    private String description;
    private String intentAction;
    private int resultCode;

    public Demo(String title, String description){
        this(title, description, null, 0);
    }

    public Demo(String title, String description, String action, int resultCode){
        this.title = title;
        this.description = description;
        this.intentAction = action;
        this.resultCode = resultCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIntentAction() {
        return intentAction;
    }

    public void setIntentAction(String intentAction) {
        this.intentAction = intentAction;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
package com.hermansendev.l03_post_it;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SendHaikuActivity extends AppCompatActivity {

    Button sendHaikuButton;
    Button cancelButton;

    EditText phoneText;

    String HaikuText;

    private static final int PERMISSION_REQUEST_SEND_SMS = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_haiku);

        sendHaikuButton = findViewById(R.id.btn_sendHaiku_sendHaiku);
        cancelButton = findViewById(R.id.btn_sendHaiku_cancel);
        phoneText = findViewById(R.id.phone_SendHaiku);

        Intent i = getIntent();
        if (i.hasExtra(ViewActivity.HAIKU_TEXT)) {
            HaikuText = i.getExtras().getString(ViewActivity.HAIKU_TEXT);
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

        sendHaikuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendHaiku();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    private void sendHaiku() {
        checkForSmsPermission();

        String number = phoneText.getText().toString();
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", HaikuText);
        startActivity(i);
    }

    private void checkForSmsPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS )
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS},
                    PERMISSION_REQUEST_SEND_SMS);
        }
    }

    private void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

package com.hermansendev.l03_post_it;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ViewActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_EDIT_HAIKU = 100;
    public static final int REQUEST_CODE_SEND_HAIKU = 200;
    public static final String HAIKU_TEXT = "haikuText";

    Button editTextButton;
    Button sendHaikuButton;

    TextView haikuTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        editTextButton = findViewById(R.id.btn_editText);
        sendHaikuButton = findViewById(R.id.btn_send_haiku);
        haikuTextView = findViewById(R.id.txtView_haiku);

        editTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editHaiku();
            }
        });
        sendHaikuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendHaiku();
            }
        });
    }

    private void editHaiku() {
        Intent i = new Intent(this, EditActivity.class);

        if (haikuTextView.getText() != "") {
            i.putExtra(HAIKU_TEXT, haikuTextView.getText().toString());
        }

        startActivityForResult(i, REQUEST_CODE_EDIT_HAIKU);
    }

    private void sendHaiku(){
        Intent i = new Intent(this, SendHaikuActivity.class);

        if (haikuTextView.getText() != "") {
            i.putExtra(HAIKU_TEXT, haikuTextView.getText().toString());
        }

        startActivityForResult(i, REQUEST_CODE_SEND_HAIKU);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EDIT_HAIKU) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(HAIKU_TEXT)) {
                    haikuTextView.setText(data.getExtras().getString(HAIKU_TEXT));
                }
            }
        }
        else if (requestCode == REQUEST_CODE_SEND_HAIKU) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Your Message Has been sent", Toast.LENGTH_LONG).show();
            }
        }
    }
}

package com.hermansendev.l03_post_it;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {



    Button acceptBtn;
    Button cancelBtn;

    EditText haikuText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        acceptBtn = findViewById(R.id.btn_accept);
        cancelBtn = findViewById(R.id.btn_cancel);

        haikuText = findViewById(R.id.txtField_edit_text);

        Intent i = getIntent();
        if (i.hasExtra(ViewActivity.HAIKU_TEXT)) {
            haikuText.setText(i.getExtras().getString(ViewActivity.HAIKU_TEXT));
        }
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptText();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelText();
            }
        });
    }

    private void acceptText()
    {
        Intent data = new Intent();
        data.putExtra(ViewActivity.HAIKU_TEXT, haikuText.getText().toString());
        setResult(RESULT_OK, data);
        finish();
    }

    private void cancelText()
    {
        setResult(RESULT_CANCELED);
        finish();
    }
}

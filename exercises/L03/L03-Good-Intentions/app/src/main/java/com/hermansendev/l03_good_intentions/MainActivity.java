
package com.hermansendev.l03_good_intentions;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_SWITCH_ACTIVITY = 0;

    Button switchViewBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switchViewBtn = findViewById(R.id.btn_GoToSecondActivity);

        switchViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchActivity();
            }
        });
    }

    private void switchActivity() {
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }
}

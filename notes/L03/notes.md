# L03 Notes

## Lecture

### Intent

Messaging object

Communication between objects

Starting Activities etc
Start Service
Send broadcast Intent

#### Two types

- Explicit: Fully qualified "I choose you, Instagram Camera"
- Implicit: General action: "I need a camera"

The os will find the components

Implicit will usually display options for the user to choose "Choose a document editor to open this file"

The intent itself is passed through the framework.

Contains key:store kind of storage mechanism.

Send a mail with Gmail with a prebuilt email in the app.

#### Intent filters

Intent filters are based on three aspects

- The action
- The data
- The catego

#### Intent Structure

- Component name
- Action
- Data
- Category
- Extras
- Flags

##### Data

URI
MIME

##### Extras

key/value pairs
Standard Extras keys provided by framework, e.g.

#### Get/Put data 

`putExtras(string key, Type value)`
`getTypeExtra(String key, (default value))`

#### Start an activity

`startActivity(Intent intent)`

with callback

`startActivityForResult(Intent intent, int requestCode)`

handle callback

`onActivityResult(int requestCode, int resultCode, Intent data)`

I remember to check permissions

Check if there is an activity at all

`setReuslts(int resultcode, Intent data)`

`finish`

get Intent

`Intent data  = getIntent()`

## Exercises

### 01

- Create a new application with two activities: MainActivity and SecondActivity
- Create a Button in your MainActivity UI that takes the user to SecondActivity
- SecondACtivity must have a Button where the user returns to the MainActivity
